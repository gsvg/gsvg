/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-svg-test.vala

 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using GXml;

public class GSvgTest.SvgTest {
  public static void add_funcs ()
  {
    Test.add_func ("/gsvg/svg/construct/default",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/svg/construct/document",
    ()=>{
      try {
        var d = new GSvg.Document ();
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"" in d.write_string ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/rect-element/construct/default",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
        var r = svg.create_rect (null, null, null, null, null, null);
        svg.append_child (r);
        s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"><rect/></svg>" in s);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/rect-element/construct/initialize",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
        var r = svg.create_rect ("0cm", "1cm",
                      "1.5cm", "1.5cm",
                      "0.1cm", "0.1cm");
        svg.append_child (r);
        s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"><rect x=\"0cm\" y=\"1cm\" width=\"1.5cm\" height=\"1.5cm\" rx=\"0.1cm\" ry=\"0.1cm\"/></svg>" in s);
        assert (r.x.base_val.value == (float) 0.0);
        assert (r.x.base_val.unit_type == DomLength.Type.CM);
        assert (r.y.base_val.value == (float) 1.0);
        assert (r.y.base_val.unit_type == DomLength.Type.CM);
        assert (r.width.base_val.value == (float) 1.5);
        assert (r.width.base_val.unit_type == DomLength.Type.CM);
        assert (r.height.base_val.value == (float) 1.5);
        assert (r.height.base_val.unit_type == DomLength.Type.CM);
        assert ("%.2f".printf (r.rx.base_val.value) == "%.2f".printf (0.1));
        assert (r.rx.base_val.unit_type == DomLength.Type.CM);
        assert ("%.2f".printf (r.ry.base_val.value) == "%.2f".printf (0.1));
        assert (r.ry.base_val.unit_type == DomLength.Type.CM);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/circle-element/construct/initialize",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new GXml.XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
        var c = svg.create_circle ("1mm", "1mm", "3.5mm");
        svg.append_child (c);
        s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"><circle cx=\"1mm\" cy=\"1mm\" r=\"3.5mm\"/></svg>" in s);
        assert ("%.2f".printf (c.cx.base_val.value) == "%.2f".printf (1.0));
        assert (c.cx.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (c.cy.base_val.value) == "%.2f".printf (1.0));
        assert (c.cy.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (c.r.base_val.value) == "%.2f".printf (3.5));
        assert (c.r.base_val.unit_type == DomLength.Type.MM);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/ellipse-element/construct/initialize",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
        var c = svg.create_ellipse ("1mm", "1mm", "3.5mm", "3.5mm");
        svg.append_child (c);
        s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"><ellipse cx=\"1mm\" cy=\"1mm\" rx=\"3.5mm\" ry=\"3.5mm\"/></svg>" in s);
        assert ("%.2f".printf (c.cx.base_val.value) == "%.2f".printf (1.0));
        assert (c.cx.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (c.cy.base_val.value) == "%.2f".printf (1.0));
        assert (c.cy.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (c.rx.base_val.value) == "%.2f".printf (3.5));
        assert (c.rx.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (c.ry.base_val.value) == "%.2f".printf (3.5));
        assert (c.ry.base_val.unit_type == DomLength.Type.MM);
        message ("cx=%s | %s", c.cx.value, c.get_attribute ("cx"));
      } catch (GLib.Error e) {
        GLib.message ("ERROR: "+e.message);
      }
    });
    Test.add_func ("/gsvg/line-element/construct/initialize",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        var parser = new XParser (svg);
        string s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"/>" in s);
        var l = svg.create_line  ("0mm", "0mm", "50mm", "50mm");
        svg.append_child (l);
        s = parser.write_string ();
        assert (s != null);
        GLib.message ("SVG: "+s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"><line x1=\"0mm\" y1=\"0mm\" x2=\"50mm\" y2=\"50mm\"/></svg>" in s);
        assert ("%.2f".printf (l.x1.base_val.value) == "%.2f".printf (0.0));
        assert (l.x1.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (l.y1.base_val.value) == "%.2f".printf (0.0));
        assert (l.y1.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (l.x2.base_val.value) == "%.2f".printf (50.0));
        assert (l.x2.base_val.unit_type == DomLength.Type.MM);
        assert ("%.2f".printf (l.y2.base_val.value) == "%.2f".printf (50.0));
        assert (l.y2.base_val.unit_type == DomLength.Type.MM);
      } catch (GLib.Error e) {
        GLib.message ("ERROR: "+e.message);
      }
    });
    Test.add_func ("/gsvg/svg-element/parse",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.read_from_string ("""<svg xmlns="http://www.w3.org/2000/svg" width="10mm" height="100mm" />""");
        string s = svg.write_string ();
        message ("SVG Text: %s", s);
        assert ("<svg xmlns=\"http://www.w3.org/2000/svg\"" in s);
        assert (svg.width.value == "10mm");
        assert (svg.height.value == "100mm");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/document",
    ()=>{
      try {
        string str = """<svg xmlns="http://www.w3.org/2000/svg" width="8.5in" viewBox="0 0 8.5 11"><line x1="0mm" y1="0mm" x2="50mm" y2="50mm"/></svg>""";
        var svg = new GSvg.Document () as GSvg.DomDocument;
        assert (svg.root_element != null);
        message (svg.write_string ());
        svg.read_from_string (str);
        message (svg.write_string ());
        assert (svg.root_element != null);
        assert (svg.root_element is DomSvgElement);
        assert (svg.root_element.width != null);
        assert (svg.root_element.width.base_val != null);
        assert (svg.root_element.width.base_val.value == 8.5);
        assert (svg.root_element.width.base_val.unit_type == DomLength.Type.IN);
        assert (svg.root_element.view_box != null);
      } catch (GLib.Error e) {
        GLib.message ("ERROR: "+e.message);
      }
    });
    Test.add_func ("/gsvg/aspect-radio",
    ()=>{
      try {
        var aar = new AnimatedPreserveAspectRatio ();
        aar.value = "xMaxYMax";
        assert (aar.base_val.align == DomPreserveAspectRatio.Type.XMAXYMAX);
        assert (aar.base_val.meet_or_slice == DomPreserveAspectRatio.MeetorSlice.UNKNOWN);
        aar.value = "defer xMaxYMax";
        assert (aar.base_val.align == DomPreserveAspectRatio.Type.XMAXYMAX);
        assert (aar.base_val.meet_or_slice == DomPreserveAspectRatio.MeetorSlice.UNKNOWN);
        aar.value = "xMaxYMax meet";
        assert (aar.base_val.align == DomPreserveAspectRatio.Type.XMAXYMAX);
        assert (aar.base_val.meet_or_slice == DomPreserveAspectRatio.MeetorSlice.MEET);
        var str = """<svg preserveAspectRatio="xMaxYMax meet" />""";
        var svg = new GSvg.SvgElement ();
        svg.read_from_string (str);
        assert (svg.preserve_aspect_ratio != null);
        assert (svg.preserve_aspect_ratio.base_val.align == DomPreserveAspectRatio.Type.XMAXYMAX);
        assert (svg.preserve_aspect_ratio.base_val.meet_or_slice == DomPreserveAspectRatio.MeetorSlice.MEET);
        assert (svg.preserve_aspect_ratio.value == "xMaxYMax meet");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/container",
    ()=>{
      try {
        string str = """<svg xmlns="http://www.w3.org/2000/svg" width="8.5in" viewBox="0 0 8.5 11"><svg id="svg"><line id="line" x1="0mm" y1="0mm" x2="50mm" y2="50mm"/></svg></svg>""";
        var svg = new GSvg.Document ();
        svg.read_from_string (str);
        var csvg = svg.get_element_by_id ("svg") as DomSvgElement;
        assert (csvg != null);
        assert (csvg is DomSvgElement);
        assert (csvg is SvgElement);
        assert (csvg.id == "svg");
        assert (((DomContainerElement) csvg).lines != null);
        assert (((DomContainerElement) csvg).lines.length == 1);
        assert (((DomContainerElement) csvg).lines.get("line") is DomLineElement);
        assert (svg.root_element.svgs != null);
        assert (svg.root_element.svgs.length == 1);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/translate/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("translate(10)");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      t.matrix.matrix.print ();
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0f);
      assert (t.matrix.f == 0.0);
      t.parse ("translate( 10)");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 0.0);
      t.parse ("translate(10 )");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 0.0);
      t.parse ("translate( 10 )");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 0.0);
      t.parse ("translate(10 20)");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      t.parse ("translate( 10 20)");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      t.parse ("translate(10 20 )");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      t.parse ("translate(10  20)");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      t.parse ("translate( 10       20 )");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      t.parse ("translate( 10,20 )");
      assert (t.ttype == DomTransform.Type.TRANSLATE);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 10.0);
      assert (t.matrix.f == 20.0);
      try {
        t = new Transform ();
        t.set_translate (20.1, 40.2);
        assert (t.ttype == DomTransform.Type.TRANSLATE);
        assert (t.matrix != null);
        assert (t.matrix.a == 1.0);
        assert (t.matrix.b == 0.0);
        assert (t.matrix.c == 0.0);
        assert (t.matrix.d == 1.0);
        assert (t.matrix.e == 20.10f);
        assert (t.matrix.f == 40.2f);
        message (t.to_string ());
        assert (t.to_string () == "translate(20.1 40.2)");
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/transform/list/translate/space",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "translate(10)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.TRANSLATE);
        assert (t.matrix != null);
        assert (t.matrix.a == 1.0);
        assert (t.matrix.b == 0.0);
        assert (t.matrix.c == 0.0);
        assert (t.matrix.d == 1.0);
        assert (t.matrix.e == 10.0);
        assert (t.matrix.f == 0.0);
        assert (str == tl.value);
        str = "translate(11 21)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.TRANSLATE);
        assert (t2.matrix != null);
        assert (t2.matrix.a == 1.0);
        assert (t2.matrix.b == 0.0);
        assert (t2.matrix.c == 0.0);
        assert (t2.matrix.d == 1.0);
        assert (t2.matrix.e == 11.0);
        assert (t2.matrix.f == 21.0);
        assert (str == tl.value);
        str = "translate(11 21) translate(20 45)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t3 = tl.base_val.get_item (0);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.TRANSLATE);
        assert (t3.matrix != null);
        assert (t3.matrix.a == 1.0);
        assert (t3.matrix.b == 0.0);
        assert (t3.matrix.c == 0.0);
        assert (t3.matrix.d == 1.0);
        assert (t3.matrix.e == 11.0);
        assert (t3.matrix.f == 21.0);
        var t4 = tl.base_val.get_item (1);
        assert (t4 != null);
        assert (t4.ttype == DomTransform.Type.TRANSLATE);
        assert (t4.matrix != null);
        assert (t4.matrix.a == 1.0);
        assert (t4.matrix.b == 0.0);
        assert (t4.matrix.c == 0.0);
        assert (t4.matrix.d == 1.0);
        assert (t4.matrix.e == 20.0);
        assert (t4.matrix.f == 45.0);
        assert (str == tl.value);
        str = "translate(11) translate(1.2) translate(11 21) translate(20.1 45) translate(12 23.3)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 5);
        var t5 = tl.base_val.get_item (0);
        assert (t5 != null);
        assert (t5.ttype == DomTransform.Type.TRANSLATE);
        assert (t5.matrix != null);
        assert (t5.matrix.a == 1.0);
        assert (t5.matrix.b == 0.0);
        assert (t5.matrix.c == 0.0);
        assert (t5.matrix.d == 1.0);
        assert (t5.matrix.e == 11.0f);
        assert (t5.matrix.f == 0.0);
        var t6 = tl.base_val.get_item (1);
        assert (t6 != null);
        assert (t6.ttype == DomTransform.Type.TRANSLATE);
        assert (t6.matrix != null);
        assert (t6.matrix.a == 1.0);
        assert (t6.matrix.b == 0.0);
        assert (t6.matrix.c == 0.0);
        assert (t6.matrix.d == 1.0);
        assert (t6.matrix.e == 1.2f);
        assert (t6.matrix.f == 0.0);
        var t7 = tl.base_val.get_item (2);
        assert (t7 != null);
        assert (t7.ttype == DomTransform.Type.TRANSLATE);
        assert (t7.matrix != null);
        assert (t7.matrix.a == 1.0);
        assert (t7.matrix.b == 0.0);
        assert (t7.matrix.c == 0.0);
        assert (t7.matrix.d == 1.0);
        assert (t7.matrix.e == 11.0);
        assert (t7.matrix.f == 21.0);
        var t8 = tl.base_val.get_item (3);
        assert (t8 != null);
        assert (t8.ttype == DomTransform.Type.TRANSLATE);
        assert (t8.matrix != null);
        assert (t8.matrix.a == 1.0);
        assert (t8.matrix.b == 0.0);
        assert (t8.matrix.c == 0.0);
        assert (t8.matrix.d == 1.0);
        assert (t8.matrix.e == 20.1f);
        assert (t8.matrix.f == 45.0);
        var t9 = tl.base_val.get_item (4);
        assert (t9 != null);
        assert (t9.ttype == DomTransform.Type.TRANSLATE);
        assert (t9.matrix != null);
        assert (t9.matrix.a == 1.0);
        assert (t9.matrix.b == 0.0);
        assert (t9.matrix.c == 0.0);
        assert (t9.matrix.d == 1.0);
        assert (t9.matrix.e == 12.0f);
        assert (t9.matrix.f == 23.3f);
        message (str);
        message (tl.value);
        assert (str == tl.value);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/list/translate/comma",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "translate(10)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.TRANSLATE);
        assert (t.matrix != null);
        assert (t.matrix.a == 1.0);
        assert (t.matrix.b == 0.0);
        assert (t.matrix.c == 0.0);
        assert (t.matrix.d == 1.0);
        assert (t.matrix.e == 10.0);
        assert (t.matrix.f == 0.0);
        assert (str == tl.value);
        str = "translate(11,21)";
        message (str);
        tl.value = str;
        message (tl.value);
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.TRANSLATE);
        assert (t2.matrix != null);
        assert (t2.matrix.a == 1.0);
        assert (t2.matrix.b == 0.0);
        assert (t2.matrix.c == 0.0);
        assert (t2.matrix.d == 1.0);
        assert (t2.matrix.e == 11.0);
        assert (t2.matrix.f == 21.0);
        assert (str == tl.value);
        str = "translate(11,21) translate(20,45)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t3 = tl.base_val.get_item (0);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.TRANSLATE);
        assert (t3.matrix != null);
        assert (t3.matrix.a == 1.0);
        assert (t3.matrix.b == 0.0);
        assert (t3.matrix.c == 0.0);
        assert (t3.matrix.d == 1.0);
        assert (t3.matrix.e == 11.0);
        assert (t3.matrix.f == 21.0);
        var t4 = tl.base_val.get_item (1);
        assert (t4 != null);
        assert (t4.ttype == DomTransform.Type.TRANSLATE);
        assert (t4.matrix != null);
        assert (t4.matrix.a == 1.0);
        assert (t4.matrix.b == 0.0);
        assert (t4.matrix.c == 0.0);
        assert (t4.matrix.d == 1.0);
        assert (t4.matrix.e == 20.0);
        assert (t4.matrix.f == 45.0);
        assert (str == tl.value);
        str = "translate(11) translate(1.2) translate(11,21) translate(20.1,45) translate(12,23.3)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 5);
        var t5 = tl.base_val.get_item (0);
        assert (t5 != null);
        assert (t5.ttype == DomTransform.Type.TRANSLATE);
        assert (t5.matrix != null);
        assert (t5.matrix.a == 1.0);
        assert (t5.matrix.b == 0.0);
        assert (t5.matrix.c == 0.0);
        assert (t5.matrix.d == 1.0);
        assert (t5.matrix.e == 11.0);
        assert (t5.matrix.f == 0.0);
        var t6 = tl.base_val.get_item (1);
        assert (t6 != null);
        assert (t6.ttype == DomTransform.Type.TRANSLATE);
        assert (t6.matrix != null);
        assert (t6.matrix.a == 1.0);
        assert (t6.matrix.b == 0.0);
        assert (t6.matrix.c == 0.0);
        assert (t6.matrix.d == 1.0);
        assert (t6.matrix.e == 1.2f);
        assert (t6.matrix.f == 0.0);
        var t7 = tl.base_val.get_item (2);
        assert (t7 != null);
        assert (t7.ttype == DomTransform.Type.TRANSLATE);
        assert (t7.matrix != null);
        assert (t7.matrix.a == 1.0);
        assert (t7.matrix.b == 0.0);
        assert (t7.matrix.c == 0.0);
        assert (t7.matrix.d == 1.0);
        assert (t7.matrix.e == 11.0f);
        assert (t7.matrix.f == 21.0f);
        var t8 = tl.base_val.get_item (3);
        assert (t8 != null);
        assert (t8.ttype == DomTransform.Type.TRANSLATE);
        assert (t8.matrix != null);
        assert (t8.matrix.a == 1.0);
        assert (t8.matrix.b == 0.0);
        assert (t8.matrix.c == 0.0);
        assert (t8.matrix.d == 1.0);
        assert (t8.matrix.e == 20.1f);
        assert (t8.matrix.f == 45.0f);
        var t9 = tl.base_val.get_item (4);
        assert (t9 != null);
        assert (t9.ttype == DomTransform.Type.TRANSLATE);
        assert (t9.matrix != null);
        assert (t9.matrix.a == 1.0);
        assert (t9.matrix.b == 0.0);
        assert (t9.matrix.c == 0.0);
        assert (t9.matrix.d == 1.0);
        assert (t9.matrix.e == 12.0);
        assert (t9.matrix.f == 23.3f);
        message (str);
        message (tl.value);
        assert (str == tl.value);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/scale/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("scale(10)");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale( 10)");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale(10 )");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale( 10 )");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 1.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale(10 20)");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale( 10 20)");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale(10 20 )");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale(10  20)");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale( 10       20 )");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      t.parse ("scale( 10,20 )");
      assert (t.ttype == DomTransform.Type.SCALE);
      assert (t.matrix != null);
      assert (t.matrix.a == 10.0);
      assert (t.matrix.b == 0.0);
      assert (t.matrix.c == 0.0);
      assert (t.matrix.d == 20.0);
      assert (t.matrix.e == 0.0);
      assert (t.matrix.f == 0.0);
      try {
        t = new Transform ();
        t.set_scale (20.1, 40.2);
        assert (t.ttype == DomTransform.Type.SCALE);
        assert (t.matrix != null);
        assert (t.matrix.a == 20.1f);
        assert (t.matrix.b == 0.0);
        assert (t.matrix.c == 0.0);
        assert (t.matrix.d == 40.2f);
        assert (t.matrix.e == 0.0);
        assert (t.matrix.f == 0.0);
        message (t.to_string ());
        assert (t.to_string () == "scale(20.1 40.2)");
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/transform/list/scale",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "scale(10)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.SCALE);
        assert (t.matrix != null);
        assert (t.matrix.a == 10.0);
        assert (t.matrix.b == 0.0);
        assert (t.matrix.c == 0.0);
        assert (t.matrix.d == 1.0);
        assert (t.matrix.e == 0.0);
        assert (t.matrix.f == 0.0);
        message (tl.value);
        assert (str == tl.value);
        str = "scale(11 21)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.SCALE);
        assert (t2.matrix != null);
        assert (t2.matrix.a == 11.0);
        assert (t2.matrix.b == 0.0);
        assert (t2.matrix.c == 0.0);
        assert (t2.matrix.d == 21.0);
        assert (t2.matrix.e == 0.0);
        assert (t2.matrix.f == 0.0);
        assert (str == tl.value);
        str = "scale(11 21) scale(20 45)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t3 = tl.base_val.get_item (0);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.SCALE);
        assert (t3.matrix != null);
        assert (t3.matrix.a == 11.0);
        assert (t3.matrix.b == 0.0);
        assert (t3.matrix.c == 0.0);
        assert (t3.matrix.d == 21.0);
        assert (t3.matrix.e == 0.0);
        assert (t3.matrix.f == 0.0);
        var t4 = tl.base_val.get_item (1);
        assert (t4 != null);
        assert (t4.ttype == DomTransform.Type.SCALE);
        assert (t4.matrix != null);
        assert (t4.matrix.a == 20.0);
        assert (t4.matrix.b == 0.0);
        assert (t4.matrix.c == 0.0);
        assert (t4.matrix.d == 45.0);
        assert (t4.matrix.e == 0.0);
        assert (t4.matrix.f == 0.0);
        assert (str == tl.value);
        str = "scale(11) scale(1.2) scale(11 21) scale(20.1 45) scale(12 23.3)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 5);
        var t5 = tl.base_val.get_item (0);
        assert (t5 != null);
        assert (t5.ttype == DomTransform.Type.SCALE);
        assert (t5.matrix != null);
        assert (t5.matrix.a == 11.0f);
        assert (t5.matrix.b == 0.0);
        assert (t5.matrix.c == 0.0);
        assert (t5.matrix.d == 1.0f);
        assert (t5.matrix.e == 0.0);
        assert (t5.matrix.f == 0.0);
        var t6 = tl.base_val.get_item (1);
        assert (t6 != null);
        assert (t6.ttype == DomTransform.Type.SCALE);
        assert (t6.matrix != null);
        assert (t6.matrix.a == 1.2f);
        assert (t6.matrix.b == 0.0);
        assert (t6.matrix.c == 0.0);
        assert (t6.matrix.d == 1.0f);
        assert (t6.matrix.e == 0.0);
        assert (t6.matrix.f == 0.0);
        var t7 = tl.base_val.get_item (2);
        assert (t7 != null);
        assert (t7.ttype == DomTransform.Type.SCALE);
        assert (t7.matrix != null);
        assert (t7.matrix.a == 11.0f);
        assert (t7.matrix.b == 0.0);
        assert (t7.matrix.c == 0.0);
        assert (t7.matrix.d == 21.0f);
        assert (t7.matrix.e == 0.0);
        assert (t7.matrix.f == 0.0);
        var t8 = tl.base_val.get_item (3);
        assert (t8 != null);
        assert (t8.ttype == DomTransform.Type.SCALE);
        assert (t8.matrix != null);
        assert (t8.matrix.a == 20.1f);
        assert (t8.matrix.b == 0.0);
        assert (t8.matrix.c == 0.0);
        assert (t8.matrix.d == 45.0f);
        assert (t8.matrix.e == 0.0);
        assert (t8.matrix.f == 0.0);
        var t9 = tl.base_val.get_item (4);
        assert (t9 != null);
        assert (t9.ttype == DomTransform.Type.SCALE);
        assert (t9.matrix != null);
        assert (t9.matrix.a == 12.0f);
        assert (t9.matrix.b == 0.0);
        assert (t9.matrix.c == 0.0);
        assert (t9.matrix.d == 23.3f);
        assert (t9.matrix.e == 0.0);
        assert (t9.matrix.f == 0.0);
        assert (str == tl.value);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/list/combination",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        var str = "translate(11) scale(1.2) translate(11 21) scale(20.1 45) scale(12 23.3)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 5);
        var t5 = tl.base_val.get_item (0);
        assert (t5 != null);
        assert (t5.ttype == DomTransform.Type.TRANSLATE);
        assert (t5.matrix != null);
        assert (t5.matrix.a == 1.0f);
        assert (t5.matrix.b == 0.0);
        assert (t5.matrix.c == 0.0);
        assert (t5.matrix.d == 1.0f);
        assert (t5.matrix.e == 11.0f);
        assert (t5.matrix.f == 0.0);
        var t6 = tl.base_val.get_item (1);
        assert (t6 != null);
        assert (t6.ttype == DomTransform.Type.SCALE);
        assert (t6.matrix != null);
        assert (t6.matrix.a == 1.2f);
        assert (t6.matrix.b == 0.0);
        assert (t6.matrix.c == 0.0);
        assert (t6.matrix.d == 1.0f);
        assert (t6.matrix.e == 0.0);
        assert (t6.matrix.f == 0.0);
        var t7 = tl.base_val.get_item (2);
        assert (t7 != null);
        assert (t7.ttype == DomTransform.Type.TRANSLATE);
        assert (t7.matrix != null);
        assert (t7.matrix.a == 1.0f);
        assert (t7.matrix.b == 0.0);
        assert (t7.matrix.c == 0.0);
        assert (t7.matrix.d == 1.0f);
        assert (t7.matrix.e == 11.0f);
        assert (t7.matrix.f == 21.0f);
        var t8 = tl.base_val.get_item (3);
        assert (t8 != null);
        assert (t8.ttype == DomTransform.Type.SCALE);
        assert (t8.matrix != null);
        assert (t8.matrix.a == 20.1f);
        assert (t8.matrix.b == 0.0);
        assert (t8.matrix.c == 0.0);
        assert (t8.matrix.d == 45.0f);
        assert (t8.matrix.e == 0.0);
        assert (t8.matrix.f == 0.0);
        var t9 = tl.base_val.get_item (4);
        assert (t9 != null);
        assert (t9.ttype == DomTransform.Type.SCALE);
        assert (t9.matrix != null);
        assert (t9.matrix.a == 12.0f);
        assert (t9.matrix.b == 0.0);
        assert (t9.matrix.c == 0.0);
        assert (t9.matrix.d == 23.3f);
        assert (t9.matrix.e == 0.0);
        assert (t9.matrix.f == 0.0);
        //warning (tl.value);
        assert (str == tl.value);
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/rotate/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("rotate(1.5)");
      assert (t.ttype == DomTransform.Type.ROTATE);
      assert (t.angle == 1.5);
      t.parse ("rotate(1.5 100 20)");
      assert (t.ttype == DomTransform.Type.ROTATE);
      assert (t.angle == 1.5f);
      assert (t.rotation_center != null);
      assert (t.rotation_center.x != null);
      assert (t.rotation_center.x.value == 100);
      assert (t.rotation_center.y != null);
      assert (t.rotation_center.y.value == 20);
      t.parse ("rotate(1.5,100,20)");
      assert (t.ttype == DomTransform.Type.ROTATE);
      assert (t.angle == 1.5f);
      assert (t.rotation_center != null);
      assert (t.rotation_center.x != null);
      assert (t.rotation_center.x.value == 100);
      assert (t.rotation_center.y != null);
      assert (t.rotation_center.y.value == 20);
      try {
        t = new Transform ();
        t.set_rotate (6.3, 20.1, 40.2);
        assert (t.ttype == DomTransform.Type.ROTATE);
        assert (t.angle == 6.3);
        assert (t.rotation_center != null);
        assert (t.rotation_center.x != null);
        assert (t.rotation_center.x.value == 20.1);
        assert (t.rotation_center.y != null);
        assert (t.rotation_center.y.value == 40.2);
        message (t.to_string ());
        assert (t.to_string () == "rotate(6.3 20.1 40.2)");
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/transform/list/rotate",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "rotate(0.5)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.ROTATE);
        assert (t.angle == 0.5f);
        tl = new AnimatedTransformList ();
        str = "rotate(0.5) rotate(1.45)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.ROTATE);
        assert (t2.angle == 0.5f);
        var t3 = tl.base_val.get_item (1);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.ROTATE);
        message (t3.angle.to_string ());
        assert (t3.angle == 1.45);
        assert (tl.value == "rotate(0.5) rotate(1.45)");
        str = "rotate(0.5 20 40) rotate(1.45 40 35)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t4 = tl.base_val.get_item (0);
        assert (t4 != null);
        assert (t4.ttype == DomTransform.Type.ROTATE);
        assert (t4.angle == 0.5f);
        assert (t4.rotation_center != null);
        assert (t4.rotation_center.x != null);
        assert (t4.rotation_center.x.value == 20);
        assert (t4.rotation_center.y != null);
        assert (t4.rotation_center.y.value == 40);
        var t5 = tl.base_val.get_item (1);
        assert (t5 != null);
        assert (t5.ttype == DomTransform.Type.ROTATE);
        message (t5.angle.to_string ());
        assert (t5.angle == 1.45);
        assert (t5.rotation_center != null);
        assert (t5.rotation_center.x != null);
        assert (t5.rotation_center.x.value == 40);
        assert (t5.rotation_center.y != null);
        assert (t5.rotation_center.y.value == 35);
        assert (tl.value == "rotate(0.5 20 40) rotate(1.45 40 35)");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/matrix/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("matrix(1.5 3.4 2.5 1.4 3 6)");
      assert (t.ttype == DomTransform.Type.MATRIX);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.5f);
      assert (t.matrix.b == 3.4f);
      assert (t.matrix.c == 2.5f);
      assert (t.matrix.d == 1.4f);
      assert (t.matrix.e == 3);
      assert (t.matrix.f == 6);
      t.parse ("matrix(1.5,3.4,2.5,1.4,3 6)");
      assert (t.ttype == DomTransform.Type.MATRIX);
      assert (t.matrix != null);
      assert (t.matrix.a == 1.5f);
      assert (t.matrix.b == 3.4f);
      assert (t.matrix.c == 2.5f);
      assert (t.matrix.d == 1.4f);
      assert (t.matrix.e == 3);
      assert (t.matrix.f == 6);
    });
    Test.add_func ("/gsvg/transform/list/matrix",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "matrix(0.5 1 3 5 6 8)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.MATRIX);
        assert (t.matrix != null);
        t.matrix.matrix.print ();
        message ("a=%g; b=%g; c=%g; d=%g; e=%g; f=%g",
          t.matrix.a, t.matrix.b, t.matrix.c, t.matrix.d, t.matrix.e, t.matrix.f);
        assert (t.matrix.a == 0.5f);
        assert (t.matrix.b == 1);
        assert (t.matrix.c == 3);
        assert (t.matrix.d == 5);
        assert (t.matrix.e == 6);
        assert (t.matrix.f == 8);
        tl = new AnimatedTransformList ();
         str = "matrix(0.5 1 3 5 6 8) matrix(1.45 4 9 7 2 5)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.MATRIX);
        assert (t2.matrix != null);
        assert (t2.matrix.a == 0.5f);
        assert (t2.matrix.b == 1);
        assert (t2.matrix.c == 3);
        assert (t2.matrix.d == 5);
        assert (t2.matrix.e == 6);
        assert (t2.matrix.f == 8);
        var t3 = tl.base_val.get_item (1);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.MATRIX);
        assert (t3.matrix != null);
        assert (t3.matrix.a == 1.45f);
        assert (t3.matrix.b == 4);
        assert (t3.matrix.c == 9);
        assert (t3.matrix.d == 7);
        assert (t3.matrix.e == 2);
        assert (t3.matrix.f == 5);
        assert (tl.value == "matrix(0.5 1 3 5 6 8) matrix(1.45 4 9 7 2 5)");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/skewx/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("skewX(1.5)");
      assert (t.ttype == DomTransform.Type.SKEWX);
      assert (t.angle == 1.5f);
      try {
        t = new Transform ();
        t.set_skew_x (3.5f);
        assert (t.ttype == DomTransform.Type.SKEWX);
        assert (t.angle == 3.5f);
        message (t.to_string ());
        assert (t.to_string () == "skewX(3.5)");
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/transform/list/skewx",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "skewX(0.5)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.SKEWX);
        assert (t.angle == 0.5f);
        str = "skewX(0.5) skewX(-1.3)";
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.SKEWX);
        assert (t2.angle == 0.5f);
        var t3 = tl.base_val.get_item (1);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.SKEWX);
        assert (t3.angle == -1.3);
        assert (tl.value == "skewX(0.5) skewX(-1.3)");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/transform/skewy/parse",
    ()=>{
      var t = new Transform ();
      t.parse ("skewY(1.5)");
      assert (t.ttype == DomTransform.Type.SKEWY);
      assert (t.angle == 1.5f);
      try {
        t = new Transform ();
        t.set_skew_y (6.5);
        assert (t.ttype == DomTransform.Type.SKEWY);
        assert (t.angle == 6.5f);
        message (t.to_string ());
        assert (t.to_string () == "skewY(6.5)");
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/transform/list/skewy",
    ()=>{
      try {
        var tl = new AnimatedTransformList ();
        string str = "skewY(0.5)";
        message (str);
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 1);
        var t = tl.base_val.get_item (0);
        assert (t != null);
        assert (t.ttype == DomTransform.Type.SKEWY);
        assert (t.angle == 0.5f);
        str = "skewY(0.5) skewY(-1.3)";
        tl.value = str;
        assert (tl.base_val != null);
        assert (tl.base_val.number_of_items == 2);
        var t2 = tl.base_val.get_item (0);
        assert (t2 != null);
        assert (t2.ttype == DomTransform.Type.SKEWY);
        assert (t2.angle == 0.5f);
        var t3 = tl.base_val.get_item (1);
        assert (t3 != null);
        assert (t3.ttype == DomTransform.Type.SKEWY);
        assert (t3.angle == -1.3);
        assert (tl.value == "skewY(0.5) skewY(-1.3)");
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvg/units",
    ()=>{
      var l = new Length ();
      l.value = 5.08;
      l.unit_type = DomLength.Type.CM;
      assert (l.value_to_specified_units (DomLength.Type.CM) == 5.08);
      assert (l.value_to_specified_units (DomLength.Type.MM) == 50.8);
      message ("%g",l.value_to_specified_units (DomLength.Type.IN));
      assert (l.value_to_specified_units (DomLength.Type.IN) == 2.0);
      l.value = 3.0;
      l.unit_type = DomLength.Type.IN;
      message ("%g", l.value_to_specified_units (DomLength.Type.CM));
      assert (l.value_to_specified_units (DomLength.Type.CM) == 7.62);
      assert ("%.2f".printf (l.value_to_specified_units (DomLength.Type.MM)) == "%.2f".printf (76.20));
      assert (l.value_to_specified_units (DomLength.Type.IN) == 3.0);
      l.value = 76.2;
      l.unit_type = DomLength.Type.MM;
      assert ("%.2f".printf (l.value_to_specified_units (DomLength.Type.CM)) == "%.2f".printf (7.62));
      assert ("%.2f".printf (l.value_to_specified_units (DomLength.Type.MM)) == "%.2f".printf (76.20));
      assert ("%.2f".printf (l.value_to_specified_units (DomLength.Type.IN)) == "3.00");
      l.value = 5.08;
      l.unit_type = DomLength.Type.CM;
      message ("%g", l.value_to_pixels_at_dpi (100.0f));
      assert ("%.2f".printf (l.value_to_pixels_at_dpi (100.0f)) == "200.00");
      l.value = 50.8;
      l.unit_type = DomLength.Type.MM;
      message ("%g", l.value_to_pixels_at_dpi (100.0f));
      assert ("%.2f".printf (l.value_to_pixels_at_dpi (100.0f)) == "200.00");
      l.value = 2.0;
      l.unit_type = DomLength.Type.IN;
      message ("%g", l.value_to_pixels_at_dpi (100.0f));
      assert ("%.2f".printf (l.value_to_pixels_at_dpi (100.0f)) == "200.00");
    });
    Test.add_func ("/gsvg/svg/width/origin",
    ()=>{
      var svg = new GSvg.SvgElement ();
      var s1 = svg.create_svg ("0mm","0mm","10mm","10mm");
      s1.id = "s1";
      svg.svgs.append (s1);
      assert (svg.calculate_width () == 10.0f);
      var l1 = svg.create_line ("0mm","0mm","20mm","0mm");
      l1.id = "l1";
      svg.lines.append (l1);
      assert (svg.calculate_width () == 20.0f);
      var r1 = svg.create_rect ("0mm","0mm","30mm","0mm", null, null);
      r1.id = "r1";
      svg.rects.append (r1);
      assert (svg.calculate_width () == 30.0f);
      var c1 = svg.create_circle ("0mm","0mm","40mm", null);
      c1.id = "c1";
      svg.circles.append (c1);
      assert (svg.calculate_width () == 40.0);
      var e1 = svg.create_ellipse ("0mm","0mm","50mm", "10mm", null);
      e1.id = "e1";
      svg.ellipses.append (e1);
      assert (svg.calculate_width () == 50.0f);
    });
    Test.add_func ("/gsvg/svg/width/fixed",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.set_attribute ("width", "3mm");
        assert (svg.calculate_width () == 3.0f);
        var s1 = svg.create_svg ("0mm","0mm","10mm","10mm");
        s1.id = "s1";
        svg.svgs.append (s1);
        message ("%g", svg.calculate_width ());
        assert (svg.calculate_width () == 3.0f);
        var l1 = svg.create_line ("0mm","0mm","20mm","0mm");
        l1.id = "l1";
        svg.lines.append (l1);
        assert (svg.calculate_width () == 3.0f);
        var r1 = svg.create_rect ("0mm","0mm","30mm","0mm", null, null);
        r1.id = "r1";
        svg.rects.append (r1);
        assert (svg.calculate_width () == 3.0f);
        var c1 = svg.create_circle ("0mm","0mm","40mm", null);
        c1.id = "c1";
        svg.circles.append (c1);
        assert (svg.calculate_width () == 3.0);
        var e1 = svg.create_ellipse ("0mm","0mm","50mm", "10mm", null);
        e1.id = "e1";
        svg.ellipses.append (e1);
        assert (svg.calculate_width () == 3.0f);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/svg/width/out-origin",
    ()=>{
      var svg = new GSvg.SvgElement ();
      var s1 = svg.create_svg ("10mm","0mm","10mm","10mm");
      s1.id = "s1";
      svg.svgs.append (s1);
      assert (svg.calculate_width () == 20.0f);
      var l1 = svg.create_line ("10mm","0mm","30mm","0mm");
      l1.id = "l1";
      svg.lines.append (l1);
      assert (svg.calculate_width () == 30.0f);
      var r1 = svg.create_rect ("20mm","0mm","40mm","0mm", null, null);
      r1.id = "r1";
      svg.rects.append (r1);
      assert (svg.calculate_width () == 60.0f);
      var c1 = svg.create_circle ("30mm","0mm","40mm", null);
      c1.id = "c1";
      svg.circles.append (c1);
      assert (svg.calculate_width () == 70.0f);
      var e1 = svg.create_ellipse ("40mm","0mm","40mm", "10mm", null);
      e1.id = "e1";
      svg.ellipses.append (e1);
      assert (svg.calculate_width () == 80.0f);
    });
    Test.add_func ("/gsvg/svg/width/no-svg",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.set_attribute ("width", "10mm");
        svg.set_attribute ("height", "20mm");
        assert (svg.calculate_width () == 10.0f);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/svg/height/origin",
    ()=>{
      var svg = new GSvg.SvgElement ();
      var s1 = svg.create_svg ("0mm","0mm","10mm","20mm");
      s1.id = "s1";
      svg.svgs.append (s1);
      assert (svg.calculate_height () == 20.0f);
      var l1 = svg.create_line ("0mm","0mm","20mm","30mm");
      l1.id = "l1";
      svg.lines.append (l1);
      assert (svg.calculate_height () == 30.0f);
      var r1 = svg.create_rect ("0mm","0mm","30mm","40mm", null, null);
      r1.id = "r1";
      svg.rects.append (r1);
      assert (svg.calculate_height () == 40.0f);
      var c1 = svg.create_circle ("0mm","mm","70mm", null);
      c1.id = "c1";
      svg.circles.append (c1);
      assert (svg.calculate_height () == 70.0f);
      var e1 = svg.create_ellipse ("0mm","0mm","10mm", "80mm", null);
      e1.id = "e1";
      svg.ellipses.append (e1);
      assert (svg.calculate_height () == 80.0f);
    });
    Test.add_func ("/gsvg/svg/height/fixed",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.set_attribute ("height", "3mm");
        assert (svg.calculate_height () == 3.0f);
        var s1 = svg.create_svg ("0mm","0mm","10mm","20mm");
        s1.id = "s1";
        svg.svgs.append (s1);
        assert (svg.calculate_height () == 3.0f);
        var l1 = svg.create_line ("0mm","0mm","20mm","30mm");
        l1.id = "l1";
        svg.lines.append (l1);
        assert (svg.calculate_height () == 3.0f);
        var r1 = svg.create_rect ("0mm","0mm","30mm","40mm", null, null);
        r1.id = "r1";
        svg.rects.append (r1);
        assert (svg.calculate_height () == 3.0f);
        var c1 = svg.create_circle ("0mm","mm","70mm", null);
        c1.id = "c1";
        svg.circles.append (c1);
        assert (svg.calculate_height () == 3.0f);
        var e1 = svg.create_ellipse ("0mm","0mm","10mm", "80mm", null);
        e1.id = "e1";
        svg.ellipses.append (e1);
        assert (svg.calculate_height () == 3.0f);
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvg/svg/height/out-origin",
    ()=>{
      var svg = new GSvg.SvgElement ();
      var s1 = svg.create_svg ("10mm","20mm","10mm","40mm");
      s1.id = "s1";
      svg.svgs.append (s1);
      assert (svg.calculate_height () == 60.0f);
      var l1 = svg.create_line ("10mm","30mm","30mm","70mm");
      l1.id = "l1";
      svg.lines.append (l1);
      assert (svg.calculate_height () == 70.0f);
      var r1 = svg.create_rect ("20mm","40mm","40mm","90mm", null, null);
      r1.id = "r1";
      svg.rects.append (r1);
      assert (svg.calculate_height () == 130.0f);
      var c1 = svg.create_circle ("0mm","40mm","100mm", null);
      c1.id = "c1";
      svg.circles.append (c1);
      assert (svg.calculate_height () == 140.0f);
      var e1 = svg.create_ellipse ("0mm","50mm","10mm", "100mm", null);
      e1.id = "e1";
      svg.ellipses.append (e1);
      assert (svg.calculate_height () == 150.0f);
    });
    Test.add_func ("/gsvg/object-properties",
    ()=>{
      var svg = new GSvg.SvgElement ();
      var s1 = svg.create_svg ("10mm","20mm","10mm","40mm");
      s1.id = "s1";
      assert (s1.attributes != null);
      message ("Attributes: %d", s1.attributes.size);
      foreach (string k in s1.attributes.keys) {
        var a = s1.attributes.get (k) as DomAttr;
        message ("Attribute: %s:%s", k, a.value);
      }
      assert (s1.attributes.size > 0);
      assert (s1.get_attribute ("id") == "s1");
      assert (s1.get_attribute ("x") == "10mm");
    });
    Test.add_func ("/gsvg/svg/pixels_per_mm",
    ()=>{
      var svg = new GSvg.SvgElement ();
      svg.pixel_unit_to_millimeter_x = 20;
      svg.pixel_unit_to_millimeter_y = 25;
      svg.screen_pixel_to_millimeter_x = 30;
      svg.screen_pixel_to_millimeter_y = 40;
      var s1 = svg.create_svg ("10mm","20mm","10mm","40mm");
      svg.svgs.append (s1);
      assert (s1.pixel_unit_to_millimeter_x == 20);
      assert (s1.pixel_unit_to_millimeter_y == 25);
      assert (s1.screen_pixel_to_millimeter_x == 30);
      assert (s1.screen_pixel_to_millimeter_y == 40);
    });
    Test.add_func ("/gsvg/svg/viewbox",
    ()=>{
      try {
        var svg = new GSvg.SvgElement ();
        svg.set_attribute ("viewBox", "100 100 100 100");
        string str = svg.write_string ();
        GLib.message (str);
        assert ("""<svg xmlns="http://www.w3.org/2000/svg" viewBox="100 100 100 100"/>""" in str);
        svg.set_attribute ("viewBox", "100.5 100.5 100.9 100.7");
        str = svg.write_string ();
        GLib.message (str);
        assert ("""<svg xmlns="http://www.w3.org/2000/svg" viewBox="100.5 100.5 100.9 100.7"/>""" in str);
      } catch (GLib.Error e) {
        warning ("ERROR: %s", e.message);
      }
    });
  }
}
