/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-radial-gradient-element-map.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.RadialGradientElementMap : GXml.HashMap, DomRadialGradientElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct { try { initialize (typeof (RadialGradientElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new DomRadialGradientElement DomRadialGradientElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomRadialGradientElement;
  }
  public new void append (DomRadialGradientElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to clip radial gradients collection: %s", e.message);
    }
  }
}
