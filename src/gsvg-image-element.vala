/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-image.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public class GSvg.ImageElement : CommonShapeElement,
                            DomURIReference,
                            DomTransformable, DomImageElement {
  // Transformable
  public DomAnimatedTransformList transform { get; set; }
  [Description (nick="::transform")]
  public AnimatedTransformList mtransform {
    get { return transform as AnimatedTransformList; }
    set { transform = value as DomAnimatedTransformList; }
  }
  // URIReference
  public DomAnimatedString href { get; set; }
  [Description (nick="::href")]
  public AnimatedString mhref {
    get { return href as AnimatedString; }
    set { href = value as DomAnimatedString; }
  }
  // ImageElement
  public DomAnimatedLength x { get; set; }
  [Description (nick="::x")]
  public AnimatedLength mx {
    get { return x as AnimatedLength; }
    set { x = value as DomAnimatedLength; }
  }
  public DomAnimatedLength y { get; set; }
  [Description (nick="::y")]
  public AnimatedLength my {
    get { return y as AnimatedLength; }
    set { y = value as DomAnimatedLength; }
  }
  public DomAnimatedLength width { get; set; }
  [Description (nick="::width")]
  public AnimatedLength mwidth {
    get { return width as AnimatedLength; }
    set { width = value as DomAnimatedLength; }
  }
  public DomAnimatedLength height { get; set; }
  [Description (nick="::height")]
  public AnimatedLength mheight {
    get { return height as AnimatedLength; }
    set { height = value as DomAnimatedLength; }
  }
  public DomAnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
  [Description (nick="::preserveAspectRatio")]
  public AnimatedPreserveAspectRatio mpreserve_aspect_ratio {
    get { return preserve_aspect_ratio as AnimatedPreserveAspectRatio; }
    set { preserve_aspect_ratio = value as DomAnimatedPreserveAspectRatio; }
  }
  construct {
    initialize ("image");
  }
}
