/* gsvg-dom-public public interfaces-filter.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * aint with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface CursorElement : GLib.Object, DomElement,
                             DomURIReference,
                             DomTests,
                             DomExternalResourcesRequired {
  public abstract DomAnimatedLength x { get; }
  public abstract DomAnimatedLength y { get; }
}

public interface DomAElement : GLib.Object, DomElement,
                        DomURIReference,
                        DomTests,
                        DomLangSpace,
                        DomExternalResourcesRequired,
                        DomStylable,
                        DomTransformable {
  public abstract DomAnimatedString target { get; }
}

public interface DomViewElement : GLib.Object, DomElement,
                           DomExternalResourcesRequired,
                           DomFitToViewBox,
                           DomZoomAndPan {
  public abstract DomStringList view_target { get; }
}

public interface DomZoomEvent : GLib.Object/*, UIEvent*/ {
  public abstract DomRect zoom_rect_screen { get; }
  public abstract double previous_scale { get; }
  public abstract DomPoint previous_translate { get; }
  public abstract double new_scale { get; }
  public abstract DomPoint new_translate { get; }
}

public interface DomElementTimeControl : GLib.Object {
  public abstract void begin_element();
  public abstract void begin_element_at(double offset);
  public abstract void end_element();
  public abstract void end_element_at(double offset);
}

public interface DomTimeEvent : GLib.Object, GXml.DomEvent {

// TODO: This is not devined in DOM4
//  public abstract AbstractView view { get; }
  public abstract int detail { get; }
// TODO: This is not devined in DOM4
//  void initTimeEvent(string typeArg, AbstractView viewArg, int detailArg);
}

public interface DomMPathElement : GLib.Object, DomElement,
                            DomURIReference,
                            DomExternalResourcesRequired {
}

} // GSvg
