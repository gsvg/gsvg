/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public class GSvg.Transform : GLib.Object, DomTransform {
  private bool ex = false;
  private bool ey = false;
  private DomMatrix _matrix = null;
  private string orig = null;
  public DomTransform.Type ttype { get; set; }
  public DomMatrix matrix {
    get { return _matrix; }
    set {
      _matrix = value;
      ttype = DomTransform.Type.MATRIX;
    }
  }
  public double angle { get; set; }
  public DomPointLength rotation_center { get; set; }

  public void set_type_matrix (DomMatrix m) throws GLib.Error {
    matrix = m;
  }
  public void set_translate (double tx, double ty) throws GLib.Error {
    matrix = new Matrix ();
    matrix.init (1, 0, 0, 1, tx, ty);
    matrix.matrix.print ();
    ttype = DomTransform.Type.TRANSLATE;
    if (tx != 0) {
      ex = true;
    }
    if (ty != 0) {
      ey = true;
    }
  }
  public void set_scale (double sx, double sy) throws GLib.Error {
    matrix = new Matrix ();
    matrix.init (sx, 0, 0, sy, 0, 0);
    ttype = DomTransform.Type.SCALE;
    if (sx != 0) {
      ex = true;
    }
    if (sy != 0) {
      ey = true;
    }
  }
  public void set_rotate (double angle, double cx, double cy) throws GLib.Error {
    matrix = new Matrix ();
    this.angle = angle;
    matrix.init (Math.cos (angle), Math.sin (angle), -(Math.sin (angle)), Math.cos (angle), 0, 0);
    matrix.f = 0.0;
    this.rotation_center = new PointLength ();
    this.rotation_center.x.unit_type = DomLength.Type.PX;
    this.rotation_center.y.unit_type = DomLength.Type.PX;
    this.rotation_center.x.value = cx;
    this.rotation_center.y.value = cy;
    ttype = DomTransform.Type.ROTATE;
    if (cx != 0) {
      ex = true;
    }
    if (cy != 0) {
      ey = true;
    }
  }
  public void set_skew_x (double angle) throws GLib.Error {
    matrix = new Matrix ();
    this.angle = angle;
    matrix.init (1, 0, Math.tan (angle), 1, 0, 0);
    ttype = DomTransform.Type.SKEWX;
  }
  public void set_skew_y (double angle) throws GLib.Error {
    matrix = new Matrix ();
    this.angle = angle;
    matrix.init (1, Math.tan (angle), 0, 1, 0, 0);
    ttype = DomTransform.Type.SKEWY;
  }
  public bool parse (string str) {
    orig = str;
    if (parse_translate (str)) return true;
    if (parse_scale (str)) return true;
    if (parse_rotate (str)) return true;
    if (parse_matrix (str)) return true;
    if (parse_skewx (str)) return true;
    if (parse_skewy (str)) return true;
    return false;
  }
  public string to_string () {
    if (orig != null) {
      return orig;
    }
    string s = "";
    if (ttype == DomTransform.Type.TRANSLATE && matrix != null) {
      s += "translate(";
      s += DomPoint.double_to_string (matrix.e);
      if (ey) {
        s += " ";
        s += DomPoint.double_to_string (matrix.f);
      }
      s += ")";
    }
    if (ttype == DomTransform.Type.SCALE && matrix != null) {
      s += "scale(";
      s += DomPoint.double_to_string (matrix.a);
      if (ey) {
        s += " ";
        s += DomPoint.double_to_string (matrix.d);
      }
      s += ")";
    }
    if (ttype == DomTransform.Type.ROTATE && matrix != null) {
      s += "rotate(";
      s += DomPoint.double_to_string (angle);
      if (ex) {
        s += " ";
        s += DomPoint.double_to_string (rotation_center.x.value);
      }
      if (ey) {
        s += " ";
        s += DomPoint.double_to_string (rotation_center.y.value);
      }
      s += ")";
    }
    if (ttype == DomTransform.Type.MATRIX && matrix != null) {
      s += "matrix(";
      s += DomPoint.double_to_string (matrix.a);
      s += " ";
      s += DomPoint.double_to_string (matrix.b);
      s += " ";
      s += DomPoint.double_to_string (matrix.c);
      s += " ";
      s += DomPoint.double_to_string (matrix.d);
      s += " ";
      s += DomPoint.double_to_string (matrix.e);
      s += " ";
      s += DomPoint.double_to_string (matrix.f);
      s += ")";
    }
    if (ttype == DomTransform.Type.SKEWX && matrix != null) {
      s += "skewX(";
      s += DomPoint.double_to_string (angle);
      s += ")";
    }
    if (ttype == DomTransform.Type.SKEWY && matrix != null) {
      s += "skewY(";
      s += DomPoint.double_to_string (angle);
      s += ")";
    }
    return s;
  }
  private bool parse_translate (string str) {
    if ("translate" in str.down ()) {
      string x = "";
      string y = "0";
      parse_two_coords (str, "translate", out x, out y);
      if (y == null) y = "0.0";
      try {
        set_translate (double.parse (x.strip ()), double.parse (y.strip()));
        return true;
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_scale (string str) {
    if (!("scale" in str.down ())) return false;
    string x = "";
    string y = "1.0";
    if (parse_two_coords (str, "scale", out x, out y)) {
      if (y == null || y == "") y = "1.0";
      try {
        set_scale (double.parse (x.strip ()), double.parse (y.strip()));
        return true;
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_rotate (string str) {
    if (!("rotate" in str.down ())) return false;
    string ang = "0.0";
    string x = "";
    string y = "";
    if (parse_three_vals (str, "rotate", out ang, out x, out y)) {
      try {
        set_rotate (double.parse (ang.strip ()), double.parse (x.strip ()), double.parse (y.strip()));
        return true;
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_matrix (string str) {
    if (!("matrix" in str.down ())) return false;
    string a, b, c, d, e, f;
    a = b = c = d = e = f = "";
    if (parse_six_vals (str, "matrix", out a, out b, out c, out d, out e, out f)) {
      try {
        var m = new Matrix ();
        m.a = double.parse (a.strip ());
        m.b = double.parse (b.strip ());
        m.c = double.parse (c.strip ());
        m.d = double.parse (d.strip ());
        m.e = double.parse (e.strip ());
        m.f = double.parse (f.strip ());
        set_type_matrix (m);
        return true;
      } catch (GLib.Error e) { GLib.warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_skewx (string str) {
    if (!("skewx" in str.down ())) return false;
    string ang = "";
    if (parse_one_value (str, "skewx", out ang)) {
      try {
        set_skew_x (double.parse (ang.strip ()));
        return true;
      } catch (GLib.Error e) { GLib.warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_skewy (string str) {
    if (!("skewy" in str.down ())) return false;
    string ang = "";
    if (parse_one_value (str, "skewy", out ang)) {
      try {
        set_skew_y (double.parse (ang.strip ()));
        return true;
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    }
    return false;
  }
  private bool parse_two_coords (string str, string kw,
                                out string x, out string y)
  {
      x = y = "";
      if (!("(" in str)) return false;
      if (!(")" in str)) return false;
      string vals = str.down ().replace(kw,"");
      vals = vals.replace ("(","");
      vals = vals.replace (")","");
      vals = vals.replace (","," ");
      vals = vals.strip ();
      x = vals;
      ey = false;
      if (" " in vals) {
        string[] astr = vals.split (" ");
        if (astr.length > 0)
          x = astr[0];
        if (astr.length > 1) {
          int i = 1;
          while (astr[i] == "") {
            i++;
            if (i == astr.length) break;
          }
          if (i < astr.length) {
            ey = true;
            y = astr[i];
          }
        }
      }
      return true;
  }
  private bool parse_one_value (string str, string kw, out string val)
  {
      val = "";
      if (!("(" in str)) return false;
      if (!(")" in str)) return false;
      string vals = str.down ().replace(kw,"");
      vals = vals.replace ("(","");
      vals = vals.replace (")","");
      vals = vals.strip ();
      val = vals;
      return true;
  }
  private bool parse_three_vals (string str, string kw,
                                out string v, out string x, out string y)
  {
      v = x = y = "";
      if (!("(" in str)) return false;
      if (!(")" in str)) return false;
      string vals = str.down ().replace(kw,"");
      vals = vals.replace ("(","");
      vals = vals.replace (")","");
      vals = vals.replace (","," ");
      vals = vals.strip ();
      v = vals;
      ex = false;
      if (" " in vals) {
        string[] astr = vals.split (" ");
        if (astr.length > 0)
          x = astr[0];
        if (astr.length > 1) {
          int i = 1;
          while (astr[i] == "") {
            i++;
            if (i == astr.length) break;
          }
          if (i < astr.length) {
            ex = true;
            x = astr[i];
          }
        }
        if (astr.length > 2) {
          int i = 2;
          while (astr[i] == "") {
            i++;
            if (i == astr.length) break;
          }
          if (i < astr.length) {
            ey = true;
            y = astr[i];
          }
        }
      }
      return true;
  }

  private bool parse_six_vals (string str, string kw,
                                out string a, out string b, out string c,
                                out string d, out string e, out string f)
  {
      a = b = c = d = e = f = "";
      if (!("(" in str)) return false;
      if (!(")" in str)) return false;
      string vals = str.down ().replace(kw,"");
      vals = vals.replace ("(","");
      vals = vals.replace (")","");
      vals = vals.replace (","," ");
      vals = vals.strip ();
      string[] astr = vals.split (" ");
      int vn = 1;
      for (int i = 0; i < astr.length; i++) {
        if (astr[i] != "") {
          if (vn == 1) {
            a = astr[i];
          }
          if (vn == 2) {
            b = astr[i];
          }
          if (vn == 3) {
            c = astr[i];
          }
          if (vn == 4) {
            d = astr[i];
          }
          if (vn == 5) {
            e = astr[i];
          }
          if (vn == 6) {
            f = astr[i];
          }
          vn++;
          if (vn > 6) {
            break;
          }
        }
      }
      return true;
  }
}

