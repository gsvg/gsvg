/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017, 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GSvg.AnimatedPoints : GSvg.Transformable,
                                    DomAnimatedPoints, GXml.MappeableElement
{
  private PointList _points_list = new PointList ();
  private DomPointList _animated_points;
  public DomPointList points {
    get { return mpoints; }
    set { mpoints = value as PointList; }
  }
  [Description (nick="::points")]
  public PointList mpoints {
    get {
      if (_points_list == null) _points_list = new PointList ();
      return _points_list;
    }
    set { _points_list = value; }
  }
  public DomPointList animated_points {
    get {
      if (_animated_points == null) _animated_points = new PointList () as DomPointList;
      return _animated_points;
    }
    set { _animated_points = value; }
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

