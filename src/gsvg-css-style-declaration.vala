/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-css-style-declaration.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

namespace GSvg {

// FIXME: This should be moved to its own library

/**
 * CSS interfaces according with [[https://www.w3.org/TR/SVG/]] version 1.1
 */
public class CssStyleDeclaration : GLib.Object,
                                    GXml.Property,
                                    DomCSSStyleDeclaration
{
  private Gee.HashMap<string,CSSValue> inherited = new Gee.HashMap<string,CSSValue> ();
  private Gee.HashMap<string,CSSValue> values = new Gee.HashMap<string,CSSValue> ();
  private Gee.ArrayList<string> values_list = new Gee.ArrayList<string> ();

  public string css_text { get; set; }
  public uint length { get { return values.size; } set {} }
  public CSSRule parent_rule { get; set; }

  public string get_property_value (string property_name) {
    var ret = "";
    var v = values.get (property_name);
    if (v != null) {
      ret = v.css_text;
    }
    return ret;
  }
  public CSSValue get_property_css_value (string property_name) {
    return values.get (property_name);
  }
  public string remove_property (string property_name) throws GLib.Error {
    string ret = "";
    var v = values.get (property_name);
    if (v != null) {
      ret = v.css_text;
      values.unset (property_name);
      values_list.remove (property_name);
    }
    return ret;
  }
  public string get_property_priority (string property_name) {
    return "";
  }
  public new void DomCSSStyleDeclaration.set_property (string property_name,
                                     string value,
                                     string priority) throws GLib.Error
  {
    var v = values.get (property_name);
    if (v == null) {
      v = new ValueCss ();
      values.set (property_name, v);
      values_list.add (property_name);
    }
    v.css_text = value;
    return;
  }
  public string item (int index) {
    return values_list.get (index);
  }
  // GomProperty
  public string?        value {
    owned get { return css_text; }
    set {
      css_text = value;
      parse (css_text);
    }
  }

  public bool validate_value (string? val) {
    return true; // FIXME:
  }
  // CSS Parsing
  private class CSSProperty : GLib.Object {
    public string name { get; set; }
    public CSSValue val { get; set; }
    public CSSProperty.from_values (string name, CSSValue val) {
      this.name = name;
      this.val = val;
    }
  }
  private Gee.HashMap<string, CSSProperty> parse (string str) {
    var h = new Gee.HashMap<string,CSSProperty> ();
    if (";" in str) {
      var tokens = str.split (";");
      foreach (string s in tokens) {
        string n = null;
        string val = null;
        if (parse_name_value (s, out n, out val)) {
          var v = new ValueCss ();
          v.css_text = val.strip ();
          v.css_value_type = CSSValue.Type.PRIMITIVE_VALUE;
          string k = n.strip ();
          values.set (k, v);
          values_list.add (k);
          var p = new CSSProperty.from_values (k, v);
          h.set (k, p);
        }
      }
    }
    return h;
  }
  private bool parse_name_value (string str, out string name, out string val) {
    val = name = "";
    if (!(":" in str)) {
      return false;
    }
    var stok = str.split (":");
    if (stok.length != 2) {
      return false;
    }
    name = stok[0];
    val = stok[1];
    return true;
  }
  public void inherit (string style) {
    var stl = new CssStyleDeclaration ();
    var p = stl.parse (style);
    foreach (string k in p.keys) {
      var item = p.get (k);
      inherited.set (k, item.val);
    }
  }
}

public class ValueCss : GLib.Object, CSSValue {
  public string css_text { get; set; }
  public CSSValue.Type css_value_type { get; set; }
}

} // GSvg
