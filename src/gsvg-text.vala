/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.TextContentElement : GSvg.CommonShapeElement,
                                  DomTextContentElement
{
  private DomAnimatedLength _dummy;
  private DomAnimatedEnumeration _ndummy;
  public DomAnimatedLength text_length { get { return _dummy; }  }
  public DomAnimatedEnumeration length_adjust { get { return _ndummy; }  }

  public int get_number_of_chars () { return 0; }
  public double get_computed_text_length () { return 0.0; }
  public double get_sub_string_length(int charnum, int nchars) throws GLib.Error { return 0.0; }
  public DomPoint get_start_position_of_char(int charnum) throws GLib.Error { return new Point (); }
  public DomPoint get_end_position_of_char(int charnum) throws GLib.Error { return new Point (); }
  public DomRect get_extent_of_char(int charnum) throws GLib.Error { return new Rect (); }
  public double get_rotation_of_char(int charnum) throws GLib.Error  { return 0.0; }
  public int get_char_num_at_position(DomPoint point) { return 0; }
  public void select_sub_string(int charnum, int nchars) throws GLib.Error {}
}

public class GSvg.TextPositioningElement : GSvg.TextContentElement,
                                   DomTextPositioningElement
{
  public DomAnimatedLengthList x { get; set; }
  [Description (nick="::x")]
  public AnimatedLengthList mx {
    get { return x as AnimatedLengthList; }
    set { x = value as DomAnimatedLengthList; }
  }
  public DomAnimatedLengthList y { get; set; }
  [Description (nick="::y")]
  public AnimatedLengthList my {
    get { return y as AnimatedLengthList; }
    set { y = value as DomAnimatedLengthList; }
  }
  public DomAnimatedLengthList dx { get; set; }
  [Description (nick="::dx")]
  public AnimatedLengthList mdx {
    get { return dx as AnimatedLengthList; }
    set { dx = value as DomAnimatedLengthList; }
  }
  public DomAnimatedLengthList dy { get; set; }
  [Description (nick="::dy")]
  public AnimatedLengthList mdy {
    get { return dy as AnimatedLengthList; }
    set { dy = value as DomAnimatedLengthList; }
  }
  public DomAnimatedNumberList rotate { get; set; }
  [Description (nick="::rotate")]
  public AnimatedNumberList mrotate {
    get { return rotate as AnimatedNumberList; }
    set { rotate = value as DomAnimatedNumberList; }
  }
}

public class GSvg.BaseTextElement : GSvg.TextPositioningElement,
                                   DomTransformable
{
  // Transformable
  public DomAnimatedTransformList transform { get; set; }
}
public class GSvg.TextElement : GSvg.BaseTextElement,
                                 DomTextElement, GXml.MappeableElement
{
  construct {
    try { initialize ("text"); } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  // API additions
  // ContainerElement
  private TSpanElementMap _spans_map;
  public DomTSpanElementMap spans { get { return _spans_map as DomTSpanElementMap; } }
  public TSpanElementMap spans_map {
    get {
      if (_spans_map == null)
        set_instance_property ("spans-maps");
      return _spans_map;
    }
    set {
      if (_spans_map != null) {
        try {
          clean_property_elements ("spans-maps");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _spans_map = value;
    }
  }
  private TRefElementMap _trefs_map;
  public DomTRefElementMap trefs { get { return _trefs_map as DomTRefElementMap; } }
  public TRefElementMap trefs_map {
    get {
      if (_trefs_map == null)
        set_instance_property ("trefs-maps");
      return _trefs_map;
    }
    set {
      if (_trefs_map != null) {
        try {
          clean_property_elements ("trefs-maps");
        } catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
      _trefs_map = value;
    }
  }
  /**
   * Add a new {@link GXml.DomText} at the end of child nodes
   */
  public GXml.DomText add_text (string txt) {
    GXml.DomText t = null;
    try {
      t = owner_document.create_text_node (txt);
      append_child (t);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
    return t;
  }
  public GXml.DomText create_text (string txt) {
    GXml.DomText t = null;
    try {
      t = owner_document.create_text_node (txt);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
    return t;
  }
  public DomTSpanElement add_span (string txt) {
    var ts = create_span (txt);
    try { append_child (ts); } catch (GLib.Error e) { warning ("Error: "+e.message); }
    return ts;
  }
  public DomTSpanElement create_span (string txt) {
    var ts = create_empty_span ();
    try {
      var t = owner_document.create_text_node (txt);
      ts.append_child (t);
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
    return ts;
  }
  public DomTSpanElement create_empty_span () {
    var ts = GLib.Object.new (typeof (TSpanElement),
                        "owner-document", owner_document)
                        as TSpanElement;
    return ts;
  }
  public DomTRefElement add_ref (string id_ref) {
    var ts = create_ref (id_ref);
    try { append_child (ts); } catch (GLib.Error e) { warning ("Error: "+e.message); }
    var r = new AnimatedString ();
    r.base_val = id_ref;
    ts.href = r;
    return ts;
  }
  public DomTRefElement create_ref (string id_ref) {
    var ts = GLib.Object.new (typeof (TRefElement),
                        "owner-document", owner_document)
                        as TRefElement;
    var r = new AnimatedString ();
    r.base_val = id_ref;
    ts.href = r;
    return ts;
  }
  public DomTextPathElement add_path (string path_ref, double offset) {
    var tp = create_path (path_ref, offset);
    try { append_child (tp); } catch (GLib.Error e) { warning ("Error: "+e.message); }
    return tp;
  }
  public DomTextPathElement create_path (string path_ref, double start_off_set) {
    var tp = create_empty_path ();
    tp.method = new AnimatedEnumeration ();
    tp.method.base_val = (int) TextPathMethodType.UNKNOWN;
    tp.spacing = new AnimatedEnumeration ();
    tp.spacing.base_val = (int) TextPathSpacingType.UNKNOWN;
    tp.href = new AnimatedString ();
    tp.href.base_val = path_ref;
    tp.start_off_set = new AnimatedLength ();
    tp.start_off_set.base_val.value = start_off_set;
    return tp;
  }
  public DomTextPathElement create_empty_path () {
    var tp = GLib.Object.new (typeof (TextPathElement),
                        "owner-document", owner_document)
                        as TextPathElement;
    return tp;
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.TextElementMap : GXml.HashMap, DomTextElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct { try { initialize (typeof (TextElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new DomTextElement DomTextElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomTextElement;
  }
  public new void append (DomTextElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to texts collection: %s", e.message);
    }
  }
}

public class GSvg.TSpanElement : GSvg.BaseTextElement,
                                  DomTSpanElement, GXml.MappeableElement
{
  construct {
    initialize ("tspan");
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.TSpanElementMap : GXml.HashMap, DomTSpanElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct { try { initialize (typeof (TSpanElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new DomTSpanElement DomTSpanElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomTSpanElement;
  }
  public new void append (DomTSpanElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to tspans collection: %s", e.message);
    }
  }
}

public class GSvg.TRefElement : GSvg.BaseTextElement,
                                   DomURIReference,
                                   DomTRefElement, GXml.MappeableElement
{
  construct {
    try {
      initialize ("tref");
      if (owner_document.document_element.lookup_prefix ("http://www.w3.org/1999/xlink") == null)
          owner_document.document_element.set_attribute_ns ("http://www.w3.org/2000/xmlns",
                          "xmlns:xlink",
                          "http://www.w3.org/1999/xlink");
     } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public DomAnimatedString href { get; set; }
  [Description (nick="::xlink:href")]
  public AnimatedString mhref {
    get { return href as AnimatedString; }
    set { href = value as DomAnimatedString; }
  }
  // MappeableElement
  public string get_map_key () { return id; }
}

public class GSvg.TRefElementMap : GXml.HashMap, DomTRefElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct { try { initialize (typeof (TRefElement)); } catch (GLib.Error e) { warning ("Error: "+e.message); } }
  public new DomTRefElement DomTRefElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomTRefElement;
  }
  public new void append (DomTRefElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to trefs collection: %s", e.message);
    }
  }
}
