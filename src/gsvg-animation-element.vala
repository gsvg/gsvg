/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-animation-element.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;


public class GSvg.AnimationElement : GSvg.Element,
                                      DomTests,
                                      DomExternalResourcesRequired,
                                      DomElementTimeControl,
                                      DomAnimationElement
{
  // ElementTimeControl
  public void begin_element () {}
  public void begin_element_at (double offset) {}
  public void end_element () {}
  public void end_element_at (double offset) {}
  // Tests
  protected DomStringList _required_features;
  protected DomStringList _required_extensions;
  protected DomStringList _system_language;
    // requiredFeatures
  public DomStringList required_features { get { return _required_features;} }
  // requiredExtensions
  public DomStringList required_extensions { get { return _required_extensions; } }
  // systemLanguage
  public DomStringList system_language { get { return _system_language; } }

  public bool has_extension (string extension) { return false; }

  // ExternalResourcesRequired
  protected DomAnimatedBoolean _external_resources_required;
  // externalResourcesRequired
  public DomAnimatedBoolean external_resources_required {
    get { return _external_resources_required; }
  }
  // AnimationElement
  private DomElement _target_element;
  public DomElement target_element { get { return _target_element; } }

  public double get_start_time ()throws GLib.Error { return 0.0; }
  public double get_current_time () { return 0.0; }
  public double get_simple_duration ()throws GLib.Error { return 0.0;}
}
