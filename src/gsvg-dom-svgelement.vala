/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016-2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;

/**
 * Top level SVG Element node according with [[https://www.w3.org/TR/SVG/]] version 1.1
 */
public interface GSvg.DomSvgElement : GLib.Object,
                          DomElement,
                          DomTests,
                          DomLangSpace,
                          DomExternalResourcesRequired,
                          DomStylable,
                          DomLocatable,
                          DomFitToViewBox,
                          DomZoomAndPan,
                          ViewCSS,
                          DocumentCSS,
                          DomContainerElement {

  public abstract DomAnimatedLength x { get; set; }
  public abstract DomAnimatedLength y { get;  set; }
  public abstract DomAnimatedLength width { get;  set; }
  public abstract DomAnimatedLength height { get;  set; }
  public abstract string content_script_type { get; set; }
  public abstract string content_style_type { get; set; }
  public abstract DomRect viewport { get; set; }
  /**
   * Size of a pixel units (as defined by CSS2) along
   * the x-axis of the viewport, which represents a unit
   * somewhere in the range of 70dpi to 120dpi, and, on
   * systems that support this, might actually match
   * the characteristics of the target medium. On systems
   * where it is impossible to know the size of a
   * pixel, a suitable default pixel size is provided.
   */
  public abstract double pixel_unit_to_millimeter_x { get; set; }
  /**
   * Corresponding size of a pixel unit along the y-axis of the viewport.
   */
  public abstract double pixel_unit_to_millimeter_y { get; set; }
  /**
   * User interface (UI) events in DOM Level 2 indicate the screen
   * positions at which the given UI event occurred. When the user
   * agent actually knows the physical size of a "screen unit",
   * this attribute will express that information; otherwise,
   * user agents will provide a suitable default value such as .28mm.
   */
  public abstract double screen_pixel_to_millimeter_x { get; set; }
  /**
   * Corresponding size of a screen pixel along the y-axis of the viewport.
   */
  public abstract double screen_pixel_to_millimeter_y { get; set; }
  /**
   * The initial view (i.e., before magnification and panning) of the
   * current innermost SVG document fragment can be either the "standard"
   * view (i.e., based on attributes on the 'svg' element such as
   * fitBoxToViewport) or to a "custom" view (i.e., a hyperlink into a
   * particular 'view' or other element - see Linking into SVG content: URI
   * fragments and SVG views). If the initial view is the "standard"
   * view, then this attribute is false. If the initial view is a "custom"
   * view, then this attribute is true.
   *
   * Exceptions on setting:
   * DOMException NO_MODIFICATION_ALLOWED_ERR: Raised on an attempt to change
   * the value of a readonly attribute.
   */
  public abstract bool use_current_view { get; }
  public abstract DomViewSpec current_view { get; }
  public abstract double current_scale { get; set; }
  public abstract DomPoint current_translate { get; }

  public abstract uint suspend_redraw (uint maxWaitMilliseconds);
  public abstract void unsuspend_redraw (uint suspendHandleID);
  public abstract void unsuspend_redrawAll ();
  public abstract void force_redraw ();
  public abstract void pause_animations ();
  public abstract void unpause_animations ();
  public abstract bool animations_paused ();
  public abstract double get_current_time ();
  public abstract void set_current_time (double seconds);
  public abstract GXml.DomNodeList get_intersection_list (DomRect rect, GSvg.DomElement referenceElement);
  public abstract GXml.DomNodeList get_enclosure_list (DomRect rect, GSvg.DomElement referenceElement);
  public abstract bool check_intersection (GSvg.DomElement element, DomRect rect);
  public abstract bool check_enclosure (GSvg.DomElement element, DomRect rect);
  public abstract void deselect_all ();
  public abstract DomNumber create_svg_number ();
  public abstract DomLength create_svg_length ();
  public abstract DomAngle create_svg_angle ();
  public abstract DomPoint create_svg_point ();
  public abstract DomMatrix create_svg_matrix ();
  public abstract DomRect create_svg_rect ();
  public abstract DomTransform create_svg_transform ();
  public abstract DomTransform create_svg_transform_from_matrix (DomMatrix matrix);
  public abstract DomElement? get_element_by_id (string elementId);
  // Shapes creation
  /**
   * Creates a 'rect' node for rectangular shapes.
   *
   * @param x a string representation of an {@link DomAnimatedLengthX}
   * @param y a string representation of an {@link DomAnimatedLengthY}
   * @param width a string representation of an {@link DomAnimatedLengthWidth}
   * @param height a string representation of an {@link DomAnimatedLengthHeight}
   * @param rx a string representation of an {@link DomAnimatedLengthRX}
   * @param ry a string representation of an {@link DomAnimatedLengthRY}
   */
  public abstract DomRectElement create_rect (string? x,
                                  string? y,
                                  string? width,
                                  string? height,
                                  string? rx,
                                  string? ry,
                                  string? style = null);
  /**
   * Creates a 'circle' node for circle shapes.
   *
   * @param cx a string representation of an {@link DomAnimatedLengthCX}
   * @param cy a string representation of an {@link DomAnimatedLengthCY}
   * @param cr a string representation of an {@link DomAnimatedLengthR}
   * @param style a string with style information
   */
  public abstract DomCircleElement create_circle (string? cx,
                                  string? cy,
                                  string? cr,
                                  string? style = null);
  /**
   * Creates a 'ellipse' node for ellipse shapes.
   *
   * @param cx a string representation of an {@link DomAnimatedLengthCX}
   * @param cy a string representation of an {@link DomAnimatedLengthCY}
   * @param crx a string representation of an {@link DomAnimatedLengthRX}
   * @param cry a string representation of an {@link DomAnimatedLengthRY}
   * @param style a string with style information
   */
  public abstract DomEllipseElement create_ellipse (string? cx,
                                  string? cy,
                                  string? crx,
                                  string? cry,
                                  string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param lx1 a string representation of an {@link DomAnimatedLengthCX}
   * @param lx2 a string representation of an {@link DomAnimatedLengthCY}
   * @param lx1 a string representation of an {@link DomAnimatedLengthRX}
   * @param ly2 a string representation of an {@link DomAnimatedLengthRY}
   * @param style a string with style information
   */
  public abstract DomLineElement create_line (string? lx1,
                                  string? ly1,
                                  string? lx2,
                                  string? ly2,
                                  string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param points a string representation of a list of {@link DomPoint}
   * @param style a string with style information
   */
  public abstract DomPolylineElement create_polyline (string points,
                                   string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param points a string representation of a list of {@link DomPoint}
   * @param style a string with style information
   */
  public abstract DomPolygonElement create_polygon (string points,
                                   string? style = null);
  /**
   * Creates a 'line' node for line shapes.
   *
   * @param text a text to be displayed
   * @param xs a list of coordinates
   * @param ys a list of coordinates
   * @param dxs a list of coordinates
   * @param dys a list of coordinates
   * @param rotates a list of numbers
   * @param style a string with style information
   */
  public abstract DomTextElement create_text (string? text,
                                   string? xs,
                                   string? ys,
                                   string? dxs,
                                   string? dys,
                                   string? rotates,
                                   string? style = null);
  /**
   * Creates a 'g' node.
   */
  public abstract DomGElement create_g ();
  /**
   * Adds a definitions element node.
   */
  public abstract DomDefsElement add_defs ();
  /**
   * Adds a grouping element node.
   */
  public abstract DomGElement add_g ();
  /**
   * Adds a 'title' node to current SVG one.
   *
   * @param text a title to be added
   */
  public abstract DomTitleElement add_title (string text);
  /**
   * Adds a description 'desc' node to current SVG one.
   *
   * @param text a text to be added to description or null, if custome values will be added
   */
  public abstract DomDescElement add_desc (string? text);
  /**
   * Adds a 'metadata' node to current SVG one.
   */
  public abstract DomMetadataElement add_metadata ();

  /**
   * Adds an 'svg' element to the this.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link DomAnimatedLength}, for x position, or null
   * @param y a string representation of {@link DomAnimatedLength}, for y position, or null
   * @param width a string representation of {@link DomAnimatedLength}, for width, or null
   * @param height a string representation of {@link DomAnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link DomAnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract DomSvgElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'svg' element. It then can be appended as a child
   * of container elements.
   *
   * If you plan to set more complex descriptions, set @param desc to null.
   *
   * @param x a string representation of {@link DomAnimatedLength}, for x position, or null
   * @param y a string representation of {@link DomAnimatedLength}, for y position, or null
   * @param width a string representation of {@link DomAnimatedLength}, for width, or null
   * @param height a string representation of {@link DomAnimatedLength}, for height, or null
   * @param viewbox a string representation of {@link DomAnimatedRect}, or null
   * @param title a string for SVG title, or null
   * @param desc a string for a text description, or null
   */
  public abstract DomSvgElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null);
  /**
   * Creates a detached 'path' element. It then can be appended as a child
   * of container elements.
   *
   * @param d a string representation path's defintion
   * @param length a string representation of {@link AnimatedNumber}, for path length, or null
   */
  public abstract DomPathElement create_path (string d, string? length, string? style = null);
  /**
   * Moves an element to given position, only if it has an x and y like properties.
   */
  public abstract void move (GSvg.DomElement e, DomPointLength p);
  /**
   * Moves a delta distance of element, only if it has an x and y like properties.
   * Current x and y units are used as type.
   */
  public abstract void move_delta (GSvg.DomElement e, double x, double y);
  /**
   * Iterates over SVGs, lines and rectangles, to find minimum width, in milimeters, required to show
   * all of them. FIXME: No transformations are considered.
   */
  public virtual double calculate_width () {
    double w = 0.0;
    if (width != null) {
      return convert_length_x (width.base_val);;
    }
    foreach (string key in ((GXml.HashMap) svgs).get_keys ()) {
      var s = svgs.get (key) as DomSvgElement;
      if (s == null) {
        continue;
      }
      double sw = 0.0;
      if (s.width != null) {
        sw = s.width.base_val.value;
        if (s.x != null) {
          sw += s.x.base_val.value;
        }
        if (sw > w) {
          w = sw;
        }
      } else {
        sw = s.calculate_width ();
        if (sw > w) {
          w = sw;
        }
      }
    }
    foreach (string key in ((GXml.HashMap) lines).get_keys ()) {
      var l = lines.get (key) as DomLineElement;
      if (l == null) {
        continue;
      }
      double lw = 0.0;
      if (l.x1 != null && l.x2 != null) {
        if (l.x2.base_val.value > l.x1.base_val.value) {
          lw = l.x2.base_val.value;
        } else {
          lw = l.x1.base_val.value;
        }
      }
      if (lw > w) {
        w = lw;
      }
    }
    foreach (string key in ((GXml.HashMap) rects).get_keys ()) {
      var r = rects.get (key) as DomRectElement;
      if (r == null) {
        continue;
      }
      double rw = 0.0;
      if (r.width != null) {
        rw = r.width.base_val.value;
      }
      if (r.x != null) {
        rw += r.x.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    foreach (string key in ((GXml.HashMap) circles).get_keys ()) {
      var c = circles.get (key) as DomCircleElement;
      if (c == null) {
        continue;
      }
      double rw = 0.0;
      if (c.r != null) {
        rw = c.r.base_val.value;
      }
      if (c.cx != null) {
        rw += c.cx.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    foreach (string key in ((GXml.HashMap) ellipses).get_keys ()) {
      var e = ellipses.get (key) as DomEllipseElement;
      if (e == null) {
        continue;
      }
      double rw = 0.0;
      if (e.rx != null) {
        rw = e.rx.base_val.value;
      }
      if (e.cx != null) {
        rw += e.cx.base_val.value;
      }
      if (rw > w) {
        w = rw;
      }
    }
    return w;
  }
  /**
   * Iterates over SVGs, lines and rectangles, to find minimum height, in milimeters, required to show
   * all of them. FIXME: No transformations are considered.
   */
  public virtual double calculate_height () {
    double h = 0.0;
    if (height != null) {
      return convert_length_y (height.base_val);
    }
    foreach (string key in ((GXml.HashMap) svgs).get_keys ()) {
      var s = svgs.get (key) as DomSvgElement;
      if (s == null) {
        continue;
      }
      double sh = 0.0;
      if (s.height != null) {
        sh = s.height.base_val.value;
        if (s.y != null) {
          sh += s.y.base_val.value;
        }
        if (sh > h) {
          h = sh;
        }
      } else {
        sh = s.calculate_height ();
        if (sh > h) {
          h = sh;
        }
      }
    }
    foreach (string key in ((GXml.HashMap) lines).get_keys ()) {
      var l = lines.get (key) as DomLineElement;
      if (l == null) {
        continue;
      }
      double lh = 0.0;
      if (l.y1 != null && l.y2 != null) {
        if (l.y2.base_val.value > l.y1.base_val.value) {
          lh = l.y2.base_val.value;
        } else {
          lh = l.y1.base_val.value;
        }
      }
      if (lh > h) {
        h = lh;
      }
    }
    foreach (string key in ((GXml.HashMap) rects).get_keys ()) {
      var r = rects.get (key) as DomRectElement;
      if (r == null) {
        continue;
      }
      double rh = 0.0;
      if (r.height != null) {
        rh = r.height.base_val.value;
      }
      if (r.y != null) {
        rh += r.y.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    foreach (string key in ((GXml.HashMap) circles).get_keys ()) {
      var c = circles.get (key) as DomCircleElement;
      if (c == null) {
        continue;
      }
      double rh = 0.0;
      if (c.r != null) {
        rh = c.r.base_val.value;
      }
      if (c.cy != null) {
        rh += c.cy.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    foreach (string key in ((GXml.HashMap) ellipses).get_keys ()) {
      var e = ellipses.get (key) as DomEllipseElement;
      if (e == null) {
        continue;
      }
      double rh = 0.0;
      if (e.ry != null) {
        rh = e.ry.base_val.value;
      }
      if (e.cy != null) {
        rh += e.cy.base_val.value;
      }
      if (rh > h) {
        h = rh;
      }
    }
    return h;
  }
  /**
   * Convert a {@link GSvg.DomLength}'s value to milimeters.
   *
   * By default units are in pixels or porcentages, the value is not converted.
   * Percentage is represented in per unit (value / 100);
   *
   * Convertion equivalencies were taken from: [[CSS]] [[https://www.w3.org/TR/css-values-4/#absolute-lengths]]
   *
   * @param l a {@link DomLength} to convert to milimeters
   * @param ppmm pixels per militers
   */
  public double convert_length (DomLength l, double ppmm) {
    double res = 0.0;
    if (l.unit_type == DomLength.Type.EMS || l.unit_type == DomLength.Type.EXS) {
      if (parent_node != null) {
        if (parent_node is GSvg.DomElement) {
          var val = get_style_property_value ("font-size");
          var fl = new AnimatedLength ();
          fl.value = val;
          double cfl = 0.0;
          if (fl.base_val.unit_type != DomLength.Type.EMS) {
            cfl = convert_length (fl.base_val, ppmm);
          }
          res = l.value * cfl;
        }
      }
      if (l.unit_type == DomLength.Type.EXS) {
        res = 0.5 * res;
      }
    } else {
      res = l.value_to_specified_units (DomLength.Type.MM, ppmm * 25.4);
    }
    return res;
  }
  /**
   * Convert a {@link GSvg.DomLength}'s value to milimeters,
   * using {@link pixel_unit_to_millimeter_x}
   *
   * Convertion equivalencies were taken from: [[CSS]] [[https://www.w3.org/TR/css-values-4/#absolute-lengths]]
   */
  public double convert_length_x (DomLength l) {
    return convert_length (l, 1 / pixel_unit_to_millimeter_x);
  }
  /**
   * Convert a {@link GSvg.Length}'s value to milimeters,
   * using {@link pixel_unit_to_millimeter_y}
   *
   * Convertion equivalencies were taken from: [[CSS]] [[https://www.w3.org/TR/css-values-4/#absolute-lengths]]
   */
  public double convert_length_y (DomLength l) {
    return convert_length (l, 1 / pixel_unit_to_millimeter_y);
  }
  /**
   * Convert a {@link Length} to pixels using convertion equivalencies
   * at: [[CSS]] [[https://www.w3.org/TR/css-values-4/#absolute-lengths]]
   *
   * @param l {@link Length} to convert
   * @param ppmm number of pixels per milimeter
   */
  public virtual double convert_length_pixels (DomLength l, double ppmm) {
    return convert_length (l, ppmm) * ppmm;
  }
  /**
   * Convert a {@link Length} to pixels using {@link pixel_unit_to_millimeter_x}
   */
  public virtual double convert_length_x_pixels (DomLength l) {
    return convert_length_pixels (l, 1 / pixel_unit_to_millimeter_x);
  }

  /**
   * Convert a {@link Length} to pixels using {@link pixel_unit_to_millimeter_y}
   */
  public virtual double convert_length_y_pixels (DomLength l) {
    return convert_length_pixels (l, 1 / pixel_unit_to_millimeter_y);
  }
  /**
   * Calculate width and heigth of this in pixels. If no width or height properties
   * exists, this method set them as -1;
   */
  public virtual void calculate_box (out double cwidth, out double cheight) {
    cwidth = -1.0;
    cheight = -1.0;
    if (width != null) {
      cwidth = convert_length_x_pixels (width.base_val);
    }
    if (height != null) {
      cheight = convert_length_y_pixels (height.base_val);
    }
  }
}

/**
 * Hash table collection of {@link DomSvgElement} elements using its
 * id as key.
 */
public interface GSvg.DomSvgElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomSvgElement get (string id);
  public abstract void append (DomSvgElement el);
}
