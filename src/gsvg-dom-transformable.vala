/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-transformable.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvg.DomTransformable : GLib.Object, DomElement, DomLocatable {
  public abstract DomAnimatedTransformList transform { get; set; }

  public ListModel transformation_matrix () throws GLib.Error {
    GLib.ListStore list = new GLib.ListStore (typeof (DomMatrix));
    if (transform != null) {
      for (int i = 0; i < transform.base_val.number_of_items; i++) {
        var t = transform.base_val.get_item (i) as DomTransform;
        if (t == null) {
          continue;
        }
        list.append (t.matrix);
        var p = this.parent_node;
        while (p != null) {
          if (p is DomTransformable) {
            var plist = ((DomTransformable) p).transformation_matrix ();
            for (int j = 0; j < plist.get_n_items (); j++) {
              list.insert (0, plist.get_item (j));
            }
          }
          p = p.parent_node;
        }
      }
    }
    return list;
  }
}
