/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

/**
 * 'svg' node.
 */
public class GSvg.SvgElement : GSvg.ContainerElement,
                        DomFitToViewBox,
                        DomZoomAndPan,
                        ViewCSS,
                        DocumentCSS,
                        GSvg.DomSvgElement, GXml.MappeableElement
{
  protected bool _use_current_view;
  protected DomViewSpec _current_view;
  protected DomPoint _current_translate;

  // FitToViewBox
  // * viewBox
  public DomAnimatedRect view_box { get; set; }
  [Description (nick="::viewBox")]
  public AnimatedRect mview_box {
    get { return view_box as AnimatedRect; }
    set { view_box = value as DomAnimatedRect; }
  }
  // preserveAspectRatio
  public DomAnimatedPreserveAspectRatio preserve_aspect_ratio { get; set; }
  [Description (nick="::preserveAspectRatio")]
  public AnimatedPreserveAspectRatio mpreserve_aspect_ratio {
    get { return preserve_aspect_ratio as AnimatedPreserveAspectRatio; }
    set { preserve_aspect_ratio = value as DomAnimatedPreserveAspectRatio; }
  }

  // ZoomAndPan
  // * zoomAndPan
  public int zoom_and_pan { get; set; }
  // ViewCSS
  public DomCSSStyleDeclaration get_computed_style (DomElement elt,
                                                      string pseudoElt)
  {
    return style; // FIXME
  }
  // DocumentCSS
  public DomCSSStyleDeclaration get_override_style (DomElement elt,
                                                string pseudoElt)
  {
    return style; // FIXME
  }
  // SVGElement
  public DomAnimatedLength x { get; set; }
  [Description (nick="::x")]
  public AnimatedLength mx {
    get { return x as AnimatedLength; }
    set { x = value as DomAnimatedLength; }
  }
  public DomAnimatedLength y { get; set; }
  [Description (nick="::y")]
  public AnimatedLength my {
    get { return y as AnimatedLength; }
    set { y = value as DomAnimatedLength; }
  }
  public DomAnimatedLength width { get; set; }
  [Description (nick="::width")]
  public AnimatedLength mwidth {
    get { return width as AnimatedLength; }
    set { width = value as DomAnimatedLength; }
  }
  public DomAnimatedLength height { get; set; }
  [Description (nick="::height")]
  public AnimatedLength mheight {
    get { return height as AnimatedLength; }
    set { height = value as DomAnimatedLength; }
  }
  [Description (nick="::contentScriptType")]
  public string content_script_type { get; set; }
  [Description (nick="::contentStyleType")]
  public string content_style_type { get; set; }
  public DomRect viewport { get; set; }
  [Description (nick="::viewport")]
  public Rect mviewport {
    get { return viewport as Rect; }
    set { viewport = value as DomRect; }
  }
  //pixelUnitToMillimeterX
  public double pixel_unit_to_millimeter_x { get; set; }
  //pixelUnitToMillimeterY
  public double pixel_unit_to_millimeter_y { get; set; }
  //screenPixelToMillimeterX
  public double screen_pixel_to_millimeter_x {  get; set; }
  //screenPixelToMillimeterY
  public double screen_pixel_to_millimeter_y {  get; set; }
  //useCurrentView
  public bool use_current_view { get { return _use_current_view; } }
  //currentView
  public DomViewSpec current_view { get { return _current_view; } }
  //currentScale
  public double current_scale { get; set; }
  // currentTranslate
  public DomPoint current_translate { get { return _current_translate; } }

  construct {
    _pixel_unit_to_millimeter_x = 96 / 25.4;
    _pixel_unit_to_millimeter_y = 96 / 25.4;
    _screen_pixel_to_millimeter_x = 96 / 25.4;
    _screen_pixel_to_millimeter_y = 96 / 25.4;
    _local_name = "svg";
    try {
      set_attribute_ns ("http://www.w3.org/2000/xmlns/",
                      "xmlns", "http://www.w3.org/2000/svg");
    } catch (GLib.Error e) {
      warning (("Error setting default namespace: %s").printf (e.message));
    }
  }

  //currentTranslate
  public  uint suspend_redraw (uint maxWaitMilliseconds) { return 0; }
  public  void unsuspend_redraw (uint suspendHandleID) {}
  public  void unsuspend_redrawAll () {}
  public  void force_redraw () {}
  public  void pause_animations () {}
  public  void unpause_animations () {}
  public  bool animations_paused () { return false; }
  public  double get_current_time () { return 0.0; }
  public  void set_current_time (double seconds) {}
  public  GXml.DomNodeList get_intersection_list (DomRect rect, GSvg.DomElement referenceElement) {
    return new GXml.NodeList () as GXml.DomNodeList;
  }
  public  GXml.DomNodeList get_enclosure_list (DomRect rect, DomElement referenceElement) {
    return new GXml.NodeList () as GXml.DomNodeList;
  }
  public  bool check_intersection (DomElement element, DomRect rect) { return false; }
  public  bool check_enclosure (DomElement element, DomRect rect) { return false; }
  public  void deselect_all () {}
  // Creators
  public  DomNumber create_svg_number () { return new Number (); }
  public  DomLength create_svg_length () { return new Length (); }
  public  DomAngle create_svg_angle () { return new Angle (); }
  public  DomPoint create_svg_point () { return new Point (); }
  public  DomMatrix create_svg_matrix() { return new Matrix (); }
  public  DomRect create_svg_rect () {
    var r = new RectElement ();
    try {
      r.set_attribute ("x", "");
      r.set_attribute ("y", "");
      r.set_attribute ("width", "");
      r.set_attribute ("height", "");
      r.set_attribute ("rx", "");
      r.set_attribute ("ry", "");
    } catch (GLib.Error e) {
      warning ("Error while creating rect: %s", e.message);
    }
    return r as DomRect;
  }
  public  DomTransform create_svg_transform () { return new Transform (); } // FIXME
  public  DomTransform create_svg_transform_from_matrix (DomMatrix matrix) { return new Transform (); } //FIXME
  /**
   * Query elements by 'id' property
   */
  public  GSvg.DomElement? get_element_by_id (string element_id) {
    try {
      var l2 = owner_document.get_element_by_id (element_id);
      if (l2 != null && l2 is GSvg.DomElement) {
        return l2 as GSvg.DomElement;
      }
      var l = get_elements_by_property_value ("id", element_id);
      if (l.length == 0) return null;
      message (l.get_element (0).node_name);

      return  l.get_element (0) as GSvg.DomElement;
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return null;
  }
  // Shape constructors
  public DomRectElement create_rect (string? x,
                                  string? y,
                                  string? width,
                                  string? height,
                                  string? rx,
                                  string? ry,
                                  string? style = null) {

    var r = GLib.Object.new (typeof (RectElement),
                        "owner_document", this.owner_document)
                        as DomRectElement;
    try  {
      if (x != null) {
        r.set_attribute ("x", x);
      }
      if (y != null) {
        r.set_attribute ("y", y);
      }
      if (width != null) {
        r.set_attribute ("width", width);
      }
      if (height != null) {
        r.set_attribute ("height", height);
      }
      if (rx != null) {
        r.set_attribute ("rx", rx);
      }
      if (ry != null) {
        r.set_attribute ("ry", ry);
      }
      if (style != null) {
        r.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating rect: %s", e.message);
    }
    return r;
  }
  public DomCircleElement create_circle (string? cx,
                                  string? cy,
                                  string? r,
                                  string? style = null) {
    var c = GLib.Object.new (typeof (CircleElement),
                        "owner_document", this.owner_document)
                        as DomCircleElement;
    try {
      if (cx != null) {
        c.set_attribute ("cx", cx);
      }
      if (cy != null) {
        c.set_attribute ("cy", cy);
      }
      if (r != null) {
        c.set_attribute ("r", r);
      }
      if (style != null) {
        c.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating circle: %s", e.message);
    }

    return c;
  }
  public DomEllipseElement create_ellipse (string? cx,
                                  string? cy,
                                  string? rx,
                                  string? ry,
                                  string? style = null) {

    var e = GLib.Object.new (typeof (EllipseElement),
                        "owner_document", this.owner_document)
                        as DomEllipseElement;
    try {
      if (cx != null) {
        e.set_attribute ("cx", cx);
      }
      if (cy != null) {
        e.set_attribute ("cy", cy);
      }
      if (rx != null) {
        e.set_attribute ("rx", rx);
      }
      if (ry != null) {
        e.set_attribute ("ry", ry);
      }
      if (style != null) {
        e.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating ellipse: %s", e.message);
    }
    return e;
  }
  public DomLineElement create_line (string? lx1,
                                  string? ly1,
                                  string? lx2,
                                  string? ly2,
                                  string? style = null) {
    var l = GLib.Object.new (typeof (LineElement),
                        "owner_document", this.owner_document)
                        as LineElement;
    try {
      if (lx1 != null) {
        l.set_attribute ("x1", lx1);
      }
      if (ly1 != null) {
        l.set_attribute ("y1", ly1);
      }
      if (lx2 != null) {
        l.set_attribute ("x2", lx2);
      }
      if (ly2 != null) {
        l.set_attribute ("y2", ly2);
      }
      if (style != null) {
        l.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating line: %s", e.message);
    }
    return l;
  }
  public DomPolylineElement create_polyline (string points,
                                   string? style = null) {
    var l = GLib.Object.new (typeof (PolylineElement),
                        "owner_document", this.owner_document)
                        as DomPolylineElement;
    l.points.value = points;
    try {
      if (style != null) {
        l.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating polyline: %s", e.message);
    }
    return l;
  }
  public DomPolygonElement create_polygon (string points,
                                   string? style = null) {
    var l = GLib.Object.new (typeof (PolygonElement),
                        "owner_document", this.owner_document)
                        as DomPolygonElement;
    l.points.value = points;
    try {
      if (style != null) {
        l.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating polygon: %s", e.message);
    }
    return l;
  }
  public DomTextElement create_text (string? text,
                                   string? x,
                                   string? y,
                                   string? dx,
                                   string? dy,
                                   string? rotate,
                                   string? style = null) {
    var t = GLib.Object.new (typeof (TextElement),
                        "owner_document", this.owner_document)
                        as TextElement;
    try {
      if (text != null)
        t.add_text (text);
      if (x != null) {
        t.set_attribute ("x", x);
      }
      if (y != null) {
        t.set_attribute ("y", y);
      }
      if (dx != null) {
        t.set_attribute ("dx", dx);
      }
      if (dy != null) {
        t.set_attribute ("dy", dy);
      }
      if (rotate != null) {
        t.set_attribute ("rotate", rotate);
      }
      if (style != null) {
        t.set_attribute ("style", style);
      }
    } catch (GLib.Error e) {
      warning ("Error while creating text: %s", e.message);
    }
    return t;
  }
  public DomGElement create_g () {
    var g = GLib.Object.new (typeof (GElement),
                        "owner_document", owner_document)
                        as GElement;
    return g;
  }
  public DomDefsElement add_defs () {
    var d = GLib.Object.new (typeof (DefsElement),
                        "owner_document", owner_document)
                        as DefsElement;
    try { append_child (d); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return d;
  }
  public DomGElement add_g () {
    var g = create_g ();
    try { append_child (g); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return g;
  }
  public DomTitleElement add_title (string text) {
    var t = GLib.Object.new (typeof (TitleElement),
                        "owner_document", owner_document)
                        as TitleElement;
    try {
      var tx = owner_document.create_text_node (text);
      t.append_child (tx);
      append_child (t);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return t;
  }
  public DomDescElement add_desc (string? text) {
    var t = GLib.Object.new (typeof (DescElement),
                        "owner_document", owner_document)
                        as DescElement;
    try {
      if (text != null) {
        var tx = owner_document.create_text_node (text);
        t.append_child (tx);
      }
      append_child (t);
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return t;
  }
  public DomMetadataElement add_metadata () {
    var mt = GLib.Object.new (typeof (MetadataElement),
                        "owner_document", owner_document)
                        as MetadataElement;
    try { append_child (mt); }
    catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return mt;
  }

  public DomSvgElement add_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    return GSvg.Document.add_svg_internal (this, x, y, width, height, viewbox, title, desc);
  }
  public DomSvgElement create_svg (string? x,
                            string? y,
                            string? width,
                            string? height,
                            string? viewbox = null,
                            string? title = null,
                            string? desc = null)
  {
    var s = GSvg.Document.create_svg_internal (owner_document, x, y, width, height, viewbox, title, desc);
    s.pixel_unit_to_millimeter_x = pixel_unit_to_millimeter_x;
    s.pixel_unit_to_millimeter_y = pixel_unit_to_millimeter_y;
    s.screen_pixel_to_millimeter_x = screen_pixel_to_millimeter_x;
    s.screen_pixel_to_millimeter_y = screen_pixel_to_millimeter_y;
    return s;
  }

  public DomPathElement create_path (string d, string? length, string? style = null) {
    AnimatedNumber l = null;
    if (length != null) {
      l = new AnimatedNumber ();
      l.value = length;
    }
    return GLib.Object.new (typeof (PathElement),
                                  "owner-document", this.owner_document,
                                  "d", d,
                                  "path-length", l, null) as DomPathElement;
  }
  public void move (GSvg.DomElement e, DomPointLength p) {
    try {
      if (e is DomSvgElement) {
        var svg = e as DomSvgElement;
        var l = new AnimatedLength ();
        l.base_val.unit_type = p.x.unit_type;
        l.base_val.value = p.x.value;
        svg.set_attribute ("x", l.value);
        l.base_val.unit_type = p.y.unit_type;
        l.base_val.value = p.y.value;
        svg.set_attribute ("y", l.value);
      }
      if (e is DomCircleElement) {
        var c = e as DomCircleElement;
        var cx = new AnimatedLengthCX ();
        cx.base_val.unit_type = p.x.unit_type;
        cx.base_val.value = p.x.value;
        c.set_attribute ("cx", cx.value);
        var cy = new AnimatedLengthCY ();
        cy.base_val.unit_type = p.y.unit_type;
        cy.base_val.value = p.y.value;
        c.set_attribute ("cy", cy.value);
      }
    } catch (GLib.Error e) {
      warning ("Error while moving element: %s", e.message);
    }
  }
  public void move_delta (GSvg.DomElement e, double dx, double dy) {
    try {
      if (e is DomSvgElement) {
        var svg = e as DomSvgElement;
        if (svg.x == null) {
          svg.set_attribute ("x", "0");
          svg.x.base_val.unit_type = DomLength.Type.MM;
        }
        svg.x.base_val.value += dx;
        if (svg.y == null) {
          svg.set_attribute ("y", "0");
          svg.y.base_val.unit_type = DomLength.Type.MM;
        }
        svg.y.base_val.value += dy;
      }
      if (e is DomCircleElement) {
        var c = e as DomCircleElement;
        if (c.cx == null) {
          c.set_attribute ("cx", "0");
          c.cx.base_val.unit_type = DomLength.Type.MM;
        }
        c.cx.base_val.value += dx;
        if (c.cy == null) {
          c.set_attribute ("cy", "0");
          c.cy.base_val.unit_type = DomLength.Type.MM;
        }
        c.cy.base_val.value += dy;
      }
    } catch (GLib.Error e) {
      warning ("Error while moving element a delta: %s", e.message);
    }
  }
  // MappeableElement
  public string get_map_key () { return id; }
  // GsElement
  public override bool pick (GSvg.DomPoint p, double delta) {
    foreach (GXml.DomNode n in child_nodes) {
      if (!(n is DomElement)) {
        continue;
      }
      if (((DomElement) n).pick (p,delta)) {
        return true;
      }
    }
    return false;
  }
}
