/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016,2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;

/**
 * Base class for SVG and basic types elements
 */
public class GSvg.CommonShapeElement : CommonElement,
                        DomLocatable
{
  // Locatable
  // nearestViewportElement
  public DomElement nearest_viewport_element { get { return _nearest_viewport_element; } }
  // farthestViewportElement
  public DomElement farthest_viewport_element { get { return _farthest_viewport_element; } }

  public DomRect get_bbox () { return new Rect (); }
  public DomMatrix get_ctm () { return new Matrix (); }
  public DomMatrix get_screen_ctm () { return new Matrix (); }
  public DomMatrix get_transform_to_element (DomElement element) throws GLib.Error { return new Matrix (); }
}