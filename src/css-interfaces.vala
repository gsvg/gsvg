/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-interfaces.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

// FIXME: This should be moved to its own library

public interface CSSValue : GLib.Object {
  public abstract string css_text { get; set; }
  public abstract Type css_value_type { get; set; }
  /**
   * UnitTypes
   */
  public enum Type {
    INHERIT= 0,
    PRIMITIVE_VALUE = 1,
    VALUE_LIST = 2,
    CUSTOM = 3
  }
}

public interface CSSRule : GLib.Object {
  public abstract uint   rtype { get; }
  public abstract string   css_text { get; set; }

  public abstract CSSStyleSheet parent_style_sheet { get; }
  public abstract CSSRule parent_rule { get; }

  // RuleType
  public enum Type {
    UNKNOWN_RULE = 0,
    STYLE_RULE = 1,
    CHARSET_RULE = 2,
    IMPORT_RULE= 3,
    MEDIA_RULE = 4,
    FONT_FACE_RULE = 5,
    PAGE_RULE= 6
  }
}

public interface CSSStyleSheet : GLib.Object {
  public abstract CSSRule owner_rule { get; }
  public abstract CSSRuleList css_rules { get; }

  public abstract uint insert_rule (string rule, uint index) throws GLib.Error;
  public abstract void delete_rule (uint index) throws GLib.Error;
}

public interface CSSRuleList : GLib.Object {
  public abstract uint length { get; }
  public abstract CSSRule item (uint index);
}


public interface DocumentCSS : GLib.Object {
  public abstract DomCSSStyleDeclaration get_override_style (DomElement elt,
                                                      string pseudoElt);
}

public interface ViewCSS : GLib.Object {
  public abstract DomCSSStyleDeclaration get_computed_style (DomElement elt,
                                                      string pseudoElt);
}

public interface CSSPrimitiveValue : GLib.Object, CSSValue {
  public abstract Type          primitive_type { get; }

  public abstract void          set_double_value (uint unit_type,
                                                double double_value) throws GLib.Error;
  public abstract double        get_double_value (uint unit_type) throws GLib.Error;
  public abstract void          set_string_value (uint string_type,
                                                  string string_value) throws GLib.Error;
  public abstract string        get_string_value () throws GLib.Error;
  public abstract CSSCounter    get_counter_value () throws GLib.Error;
  public abstract CSSRect       get_rect_value () throws GLib.Error;
  public abstract CSSRGBColor   get_rgb_color_value() throws GLib.Error;

  // UnitTypes
  public enum Type {
    UNKNOWN = 0,
    NUMBER = 1,
    PERCENTAGE = 2,
    EMS = 3,
    EXS = 4,
    PX = 5,
    CM = 6,
    MM = 7,
    IN = 8,
    PT = 9,
    PC = 10,
    DEG = 11,
    RAD = 12,
    GRAD = 13,
    MS = 14,
    S = 15,
    HZ = 16,
    KHZ = 17,
    DIMENSION = 18,
    STRING = 19,
    URI = 20,
    IDENT = 21,
    ATTR = 22,
    COUNTER = 23,
    RECT = 24,
    RGBCOLOR = 25
  }
}

/**
 *
 */
public interface CSSCounter : GLib.Object {
  public abstract string        identifier { get; }
  public abstract string        list_style { get; }
  public abstract string        separator { get; }
}

/**
 * RGB Color Definition from DOM2
 */
public interface CSSRGBColor : GLib.Object {
  public abstract CSSPrimitiveValue  red { get; }
  public abstract CSSPrimitiveValue  green { get; }
  public abstract CSSPrimitiveValue  blue { get; }
}


/**
 * Rect value taken from DOM2
 */
public interface CSSRect : GLib.Object {
  public abstract CSSPrimitiveValue  top { get; }
  public abstract CSSPrimitiveValue  right { get; }
  public abstract CSSPrimitiveValue  bottom { get; }
  public abstract CSSPrimitiveValue  left { get; }
}

/**
 * Text align properties
 *
 * Since: 0.6
 */
public enum CSSTextAlignType {
  LEFT,
  RIGHT,
  CENTER,
  JUSTIFY,
  INITIAL,
  INHERIT;
  public string to_string () {
    switch (this) {
    case LEFT:
      return "left";
    case RIGHT:
      return "right";
    case CENTER:
      return "center";
    case JUSTIFY:
      return "justify";
    case INITIAL:
      return "initial";
    case INHERIT:
      return "inherit";
    }
    return "";
  }
  public static CSSTextAlignType from_string (string str) {
    string s = str.down ();
    switch (s) {
    case "left":
      return LEFT;
    case "right":
      return RIGHT;
    case "center":
      return CENTER;
    case "justify":
      return JUSTIFY;
    case "initial":
      return INITIAL;
    case "inherit":
      return INHERIT;
    }
    return LEFT;
  }
}

} // GSvg
