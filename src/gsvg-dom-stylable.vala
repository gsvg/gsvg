/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-stylable.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvg.DomStylable : GLib.Object {
  /**
   * Represent the animatable 'class' attribute in the element
   */
  public abstract  DomAnimatedString? class_name { get; set; }
  /**
   * Represent the 'style' attribute in the element
   */
  public abstract  DomCSSStyleDeclaration? style { get; set;  }

  /**
   * Search for a style attribute in the element
   */
  public virtual CSSValue? get_presentation_attribute (string name) {
    if (style == null) {
      return null;
    }
    var v = style.get_property_css_value (name);
    if (v == null && this is GXml.DomNode) {
      var parent = ((GXml.DomNode) this).parent_node as GSvg.DomStylable;
      if (parent != null) {
        v = parent.get_presentation_attribute (name);
      }
    }
    return v;
  }
}
