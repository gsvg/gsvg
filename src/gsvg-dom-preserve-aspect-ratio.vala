/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-preserve-aspect-ratio.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public interface GSvg.DomPreserveAspectRatio : GLib.Object {
  public abstract DomPreserveAspectRatio.Type align { get; set; }
  public abstract MeetorSlice meet_or_slice { get; set; }

  // Alignment Types
  public enum Type {
    UNKNOWN = 0,
    NONE = 1,
    XMINYMIN = 2,
    XMIDYMIN = 3,
    XMAXYMIN = 4,
    XMINYMID = 5,
    XMIDYMID = 6,
    XMAXYMID = 7,
    XMINYMAX = 8,
    XMIDYMAX = 9,
    XMAXYMAX = 10;
    public static string? to_string (Type t) {
      string s = null;
      switch (t) {
        case UNKNOWN:
          break;
        case NONE:
          s = "none";
          break;
        case XMINYMIN:
          s = "xMinYMin";
          break;
        case XMIDYMIN:
          s = "xMidYMin";
          break;
        case XMAXYMIN:
          s = "xMaxYMin";
          break;
        case XMINYMID:
          s = "xMinYMid";
          break;
        case XMIDYMID:
          s = "xMidYMid";
          break;
        case XMAXYMID:
          s = "xMaxYMid";
          break;
        case XMINYMAX:
          s = "xMinYMax";
          break;
        case XMIDYMAX:
          s = "xMidYMax";
          break;
        case XMAXYMAX:
          s = "xMaxYMax";
          break;
      }
      return s;
    }
    public static Type parse (string? s) {
      if (s == null || s == "") return UNKNOWN;
      if (s.down () == "none".down ()) return NONE;
      if (s.down () == "xMinYMin".down ()) return XMINYMIN;
      if (s.down () == "xMidYMin".down ()) return XMIDYMIN;
      if (s.down () == "xMaxYMin".down ()) return XMAXYMIN;
      if (s.down () == "xMinYMid".down ()) return XMINYMID;
      if (s.down () == "xMidYMid".down ()) return XMIDYMID;
      if (s.down () == "xMaxYMid".down ()) return XMAXYMID;
      if (s.down () == "xMinYMax".down ()) return XMINYMAX;
      if (s.down () == "xMidYMax".down ()) return XMIDYMAX;
      if (s.down () == "xMaxYMax".down ()) return XMAXYMAX;
      return UNKNOWN;
    }
  }

    // Meet-or-slice Types
  public enum MeetorSlice {
    UNKNOWN = 0,
    MEET = 1,
    SLICE = 2;
    public static string? to_string (MeetorSlice m) {
      if (m == UNKNOWN) return null;
      string s = null;
      switch (m) {
        case UNKNOWN:
          break;
        case MEET:
          s = "meet";
          break;
        case SLICE:
          s = "slice";
          break;
      }
      return s;
    }
    public static MeetorSlice parse (string? str) {
      if (str == "" || str == null) return UNKNOWN;
      if (str.down () == "meet") return MEET;
      if (str.down () == "slice") return SLICE;
      return UNKNOWN;
    }
  }
}
