/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-gs-use-element.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gee;


public class GSvg.UseElement : GSvg.URIReference, DomUseElement {
  DomElementInstance _instance_root;
  DomElementInstance _animated_instance_root;

  public DomAnimatedLength x { get { return mx as DomAnimatedLength; } }
  private AnimatedLength _mx;
  [Description (nick="::x")]
  public AnimatedLength mx {
    get { return _mx; }
    set { _mx = value; }
  }
  public DomAnimatedLength y { get { return my as DomAnimatedLength; }  }
  public AnimatedLength _my;
  [Description (nick="::y")]
  public AnimatedLength my {
    get { return _my; }
    set { _my = value; }
  }
  public DomAnimatedLength width { get { return mwidth as DomAnimatedLength; }  }
  AnimatedLength _mwidth;
  [Description (nick="::width")]
  public AnimatedLength mwidth {
    get { return _mwidth; }
    set { _mwidth = value; }
  }
  public DomAnimatedLength height { get { return mheight as DomAnimatedLength; }  }
  AnimatedLength _mheight;
  [Description (nick="::height")]
  public AnimatedLength mheight {
    get { return _mheight; }
    set { _mheight = value; }
  }
  public DomElementInstance instance_root { get { return _instance_root; }  }
  public DomElementInstance animated_instance_root { get { return _animated_instance_root; }  }

  construct {
    _local_name = "use";
  }
}
