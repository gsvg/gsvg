/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-transform.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvg.DomTransform : GLib.Object {
  public abstract Type ttype { get; set; }
  public abstract DomMatrix matrix { get; set; }
  public abstract double angle { get; set; }
  public abstract DomPointLength rotation_center { get; set; }

  public abstract void set_type_matrix (DomMatrix m) throws GLib.Error;
  public abstract void set_translate (double tx, double ty) throws GLib.Error;
  public abstract void set_scale (double sx, double sy) throws GLib.Error;
  public abstract void set_rotate (double angle, double cx, double cy) throws GLib.Error;
  public abstract void set_skew_x (double angle) throws GLib.Error;
  public abstract void set_skew_y (double angle) throws GLib.Error;

  public abstract string to_string ();


  // Transform Types
  public enum Type {
    UNKNOWN = 0,
    MATRIX = 1,
    TRANSLATE = 2,
    SCALE = 3,
    ROTATE = 4,
    SKEWX = 5,
    SKEWY = 6;
  }
}
