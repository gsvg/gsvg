/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-lenght.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GXml;

public interface GSvg.DomLength : GLib.Object {
  public abstract DomLength.Type unit_type { get; construct set; }
  public abstract double value { get; set; }
  public abstract double value_in_specified_units { get; set; }
  public abstract string value_as_string { get; set; }

  public abstract void new_value_specified_units (DomLength.Type unit_type,
                                                double value_in_specified_units)
                                                throws GLib.Error;
  public abstract void convert_to_specified_units (DomLength.Type unit_type) throws GLib.Error;

  /**
   * Convert current value in current units to specified units
   * in absolute lengths.
   *
   * Equivalent units are the ones at: [[CSS]] [[https://www.w3.org/TR/css-values-4/#absolute-lengths]]
   *
   * @param units {@link DomLength.Type} type of units to convert to
   * @param dpi a value of Dots per Inch to use, defaults to 96
   */
  public virtual double value_to_specified_units (DomLength.Type units, double dpi = 96) {
    double val = value_in_specified_units;
    switch (unit_type) {
      case DomLength.Type.PERCENTAGE:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
          break;
          case DomLength.Type.EMS:
            val = 1.0;
          break;
          case DomLength.Type.EXS:
            val = 0.5;
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units / 100 * dpi;
          break;
          case DomLength.Type.PT:
            val = value_in_specified_units / 100 * 72 / 96 * dpi;
          break;
          case DomLength.Type.PC:
            val = value_in_specified_units / 100 * 6 / 96 *  dpi;
          break;
          case DomLength.Type.CM:
            val = value_in_specified_units / 100 * dpi / 2.54;
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units / 100 * 96/25.4;
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units *  dpi;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.EMS:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100;
          break;
          case DomLength.Type.EXS:
            val = value_in_specified_units * 0.5;
          break;
          case DomLength.Type.EMS:
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units * dpi;
          break;
          case DomLength.Type.PT:
            val = value_in_specified_units * 72;
          break;
          case DomLength.Type.PC:
            val = value_in_specified_units * 6;
          break;
          case DomLength.Type.CM:
          case DomLength.Type.MM:
          case DomLength.Type.IN:
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.EXS:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100;
          break;
          case DomLength.Type.EMS:
            val = value_in_specified_units / 0.5;
          break;
          case DomLength.Type.EXS:
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units * dpi * 2;
          break;
          case DomLength.Type.PT:
            val = value_in_specified_units * 72 * 2;
          break;
          case DomLength.Type.PC:
            val = value_in_specified_units * 6 * 2;
          break;
          case DomLength.Type.CM:
          case DomLength.Type.MM:
          case DomLength.Type.IN:
            val = value_in_specified_units * 2;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.PX:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100.0;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
          break;
          case DomLength.Type.EXS:
            val = 0.5;
          break;
          case DomLength.Type.PX:
          break;
          case DomLength.Type.PT:
            val = value_in_specified_units / dpi / 72;
          break;
          case DomLength.Type.PC:
            val = value_in_specified_units / dpi / 6;
          break;
          case DomLength.Type.CM:
            val = value_in_specified_units / dpi * 2.54;
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units / dpi * 25.4;
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units / dpi;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.PT:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
          break;
          case DomLength.Type.EXS:
            val = 0.5;
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units / 72 * dpi;
          break;
          case DomLength.Type.PT:
          break;
          case DomLength.Type.PC:
            val = value_in_specified_units / 72 / 6;
          break;
          case DomLength.Type.CM:
            val = value_in_specified_units / 72 * 2.54;
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units / 72 * 25.4;
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units / 72;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.PC:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
          break;
          case DomLength.Type.EXS:
            val = 0.5;
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units / 6 * dpi;
          break;
          case DomLength.Type.PT:
            val = value_in_specified_units / 6 / 72;
          break;
          case DomLength.Type.PC:
          break;
          case DomLength.Type.CM:
            val = value_in_specified_units / 6 * 2.54;
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units / 6 * 25.4;
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units / 6;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.CM:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100.0;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
            break;
          case DomLength.Type.EXS:
            val = 0.5;
            break;
          case DomLength.Type.PX:
            val = value_in_specified_units / 2.54 * dpi;
            break;
          case DomLength.Type.PT:
            val = value_in_specified_units / 2.54 / 72;
            break;
          case DomLength.Type.PC:
            val = value_in_specified_units / 2.54 / 6;
            break;
          case DomLength.Type.CM:
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units * 10.0;
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units / 2.54;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.MM:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100.0;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
            break;
          case DomLength.Type.EXS:
            val = 0.5;
            break;
          case DomLength.Type.PX:
            val = value_in_specified_units / 25.4 * dpi;
            break;
          case DomLength.Type.PT:
            val = value_in_specified_units / 25.4 / 72;
            break;
          case DomLength.Type.PC:
            val = value_in_specified_units / 25.4 / 6;
            break;
          case DomLength.Type.CM:
            val = value_in_specified_units / 10.0;
            break;
          case DomLength.Type.MM:
            break;
          case DomLength.Type.IN:
            val = value_in_specified_units / 25.4;
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.IN:
        switch (units) {
          case DomLength.Type.PERCENTAGE:
            val = 100.0;
          break;
          case DomLength.Type.EMS:
            val = 1.0;
          break;
          case DomLength.Type.EXS:
            val = 0.5;
          break;
          case DomLength.Type.PX:
            val = value_in_specified_units * dpi;
            break;
          case DomLength.Type.PT:
            val = value_in_specified_units / 72;
            break;
          case DomLength.Type.PC:
            val = value_in_specified_units / 6;
            break;
          case DomLength.Type.CM:
            val = value_in_specified_units * 2.54;
            break;
          case DomLength.Type.MM:
            val = value_in_specified_units * 25.4;
            break;
          case DomLength.Type.IN:
            break;
          case DomLength.Type.NUMBER:
          case DomLength.Type.UNKNOWN:
            break;
        }
        break;
      case DomLength.Type.NUMBER:
      case DomLength.Type.UNKNOWN:
        break;
    }
    return val;
  }
  /**
   * Convert current value in current units to pixels, scaled
   * to given Dots Per Inch (DPI).
   *
   * @param dpi Dots per inch
   */
  public virtual int value_to_pixels_at_dpi (double dpi) {
    return (int) value_to_specified_units (GSvg.DomLength.Type.PX, dpi);
  }

  /**
   * Length Unit Types
   */
  public enum Type {
    UNKNOWN = 0,
    NUMBER = 1,
    PERCENTAGE = 2,
    EMS = 3,
    EXS = 4,
    PX = 5,
    CM = 6,
    MM = 7,
    IN = 8,
    PT = 9,
    PC = 10
  }
}

