/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces-coord.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public interface GSvg.DomPointList : GLib.Object, GXml.Property {

  public abstract int   number_of_items { get; }

  public abstract void  clear() throws GLib.Error;
  public abstract DomPoint initialize (DomPoint newItem) throws GLib.Error;
  public abstract DomPoint get_item (int index) throws GLib.Error;
  public abstract DomPoint insert_item_before (DomPoint newItem, int index) throws GLib.Error;
  public abstract DomPoint replace_item (DomPoint newItem, int index) throws GLib.Error;
  public abstract DomPoint remove_item (int index) throws GLib.Error;
  public abstract DomPoint append_item (DomPoint newItem) throws GLib.Error;
  public virtual string to_string () {
    if (number_of_items == 0) return "";
    string str = "";
    for (int i = 0; i < number_of_items; i++) {
      try {
        var p = get_item (i);
        str += p.to_string ();
        if (i+1 < number_of_items) str += " ";
      } catch (GLib.Error e) {
        warning ("Error getting item: %s", e.message);
        continue;
      }
    }
    return str;
  }
}

