/* gsvg-gs-path-seg-curvetoquadratic-smooth.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



public class GSvg.PathSegCurvetoQuadraticSmooth : PathSeg {
  public double x { get; set; }
  public double y { get; set; }
}

public class GSvg.PathSegCurvetoQuadraticSmoothAbs : PathSegCurvetoQuadraticSmooth,
                                                      DomPathSegCurvetoQuadraticSmoothAbs
{}
public class GSvg.PathSegCurvetoQuadraticSmoothRel : PathSegCurvetoQuadraticSmooth,
                                                      DomPathSegCurvetoQuadraticSmoothRel
{}
