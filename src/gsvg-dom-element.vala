/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces.vala
 *
 * Copyright (C) 2016-2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GSvg.DomElement : GLib.Object, GXml.DomElement {
  public abstract string xmlbase { get; set; }
  // ownerSVGElement
  public abstract DomSvgElement? owner_svg_element { get; }
  // viewportElement
  public abstract DomElement? viewport_element { get; }

  // Styling properties
  // Fonts properties
  public abstract string font { get; set; }
  public abstract string font_family { get; set; }
  public abstract string font_size { get; set; }
  public abstract string font_size_adjust { get; set; }
  public abstract string font_stretch { get; set; }
  public abstract string font_style { get; set; }
  public abstract string font_variant { get; set; }
  public abstract string font_weight { get; set; }
  // Text properties
  public abstract string direction { get; set; }
  public abstract string letter_spacing { get; set; }
  public abstract string text_decoration { get; set; }
  public abstract string unicode_bidi { get; set; }
  public abstract string word_spacing { get; set; }
  // Other visual properties
  public abstract string clip { get; set; }
  public abstract string color { get; set; }
  public abstract string cursor { get; set; }
  public abstract string display { get; set; }
  public abstract string overflow { get; set; }
  public abstract string visibility { get; set; }
  // Clipping, Masking and Compositing properties
  public abstract string clip_path { get; set; }
  public abstract string clip_rule { get; set; }
  public abstract string mask { get; set; }
  public abstract string opacity { get; set; }
  // Filter Effects properties
  public abstract string enable_background { get; set; }
  public abstract string filter { get; set; }
  public abstract string flood_color { get; set; }
  public abstract string flood_opacity { get; set; }
  public abstract string lighting_color { get; set; }
  // Gradient properties
  public abstract string stop_color { get; set; }
  public abstract string stop_opacity { get; set; }
  // Interactivity properties
  public abstract string pointer_events { get; set; }
  // Color and painting properties
  public abstract string color_interpolation { get; set; }
  public abstract string color_interpolation_filter { get; set; }
  public abstract string color_profile { get; set; }
  public abstract string color_rendering { get; set; }
  public abstract string fill { get; set; }
  public abstract string fill_opacity { get; set; }
  public abstract string fill_rule { get; set; }
  public abstract string image_rendering { get; set; }
  public abstract string marker { get; set; }
  public abstract string maker_end { get; set; }
  public abstract string maker_mid { get; set; }
  public abstract string maker_start { get; set; }
  public abstract string shape_rendering { get; set; }
  public abstract string stroke { get; set; }
  public abstract string stroke_dasharray { get; set; }
  public abstract string stroke_dashoffset { get; set; }
  public abstract string stroke_linecap { get; set; }
  public abstract string stroke_linejoin { get; set; }
  public abstract string stroke_miterlimit { get; set; }
  public abstract string stroke_opacity { get; set; }
  public abstract string stroke_width { get; set; }
  public abstract string text_rendering { get; set; }
  // Other text properties
  public abstract string alignment_baseline { get; set; }
  public abstract string baseline_shift { get; set; }
  public abstract string dominant_baseline { get; set; }
  public abstract string glyph_orientation_horizontal { get; set; }
  public abstract string glyph_orientation_vertical { get; set; }
  public abstract string kerning { get; set; }
  public abstract string text_anchor { get; set; }
  public abstract string writing_mode { get; set; }
  // API additions
  public abstract void read_from_file (GLib.File file) throws GLib.Error;
  public abstract void read_from_uri (string uri) throws GLib.Error;
  public abstract void read_from_string (string str) throws GLib.Error;
  public abstract void read_from_stream (GLib.InputStream istream, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract string write_string () throws GLib.Error;
  public abstract void write_file (GLib.File file) throws GLib.Error;
  public abstract void write_stream (GLib.OutputStream stream) throws GLib.Error;

  public abstract async void read_from_file_async (GLib.File file, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async void read_from_uri_async (string uri, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async void read_from_string_async (string str, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async void read_from_stream_async (GLib.InputStream istream, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async string write_string_async (GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async void write_file_async (GLib.File file, GLib.Cancellable? cancellable = null) throws GLib.Error;
  public abstract async void write_stream_async (GLib.OutputStream stream, GLib.Cancellable? cancellable = null) throws GLib.Error;
  /**
   * Creates a new {link InputStream} to be used to receibe a string representation
   */
  public abstract GLib.InputStream read () throws GLib.Error;
  /**
   * Checks if a given point falls into the shape of this element.
   * This method try to make it best to find out shape boundaries.
   *
   * A {@link DomPoint} is considered to be on the shape if it is at
   * the stroke, if the shape has one, or inside the shape, if it is
   * filled.
   *
   * @param point a {@link GSvg.DomPoint} describing the point to check on
   * @param delta a value to be considered as a treshold of the point and the shape
   */
  public abstract bool pick (GSvg.DomPoint point, double delta);
  /**
   * Check if the style attribute 'stroke' is set and converts it to pixels
   */
  public abstract DomLength stroke_width_to_lenght ();

  /**
   * Translate back a point to a shape, considering
   * transformations.
   */
  public abstract DomPoint transform_point (DomPoint p) throws GLib.Error
  ;

  /**
   * Check if the style attribute 'fill' is set and is not 'none'
   */
  public virtual bool is_filled () {
    var f = get_style_property_value ("fill");
    if (f != null) {
      return f.down () != "none";
    }
    return true;
  }

  /**
   * Finds parent {@link GSvg.GElement}
   */
  public virtual GSvg.DomGElement? find_g_parent () {
    var p = parent_node as GSvg.DomElement;
    while (p != null) {
      if (p is GSvg.DomGElement) {
        return p as GSvg.DomGElement;
      }
      p = p.parent_node as GSvg.DomElement;
    }
    return null;
  }
  /**
   * Search for a style property value
   */
  public virtual string? get_style_property_value (string property_name) {
    if (this is DomStylable) {
      var v = ((DomStylable) this).get_presentation_attribute (property_name);
      if (v != null) {
        return v.css_text.down ();
      }
    }
    string val = get_attribute (property_name);
    if (val == null) {
      var pg = find_g_parent ();
      if (pg != null) {
        return pg.get_style_property_value (property_name);
      }
    }
    return val;
  }
}
