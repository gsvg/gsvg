/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-transform-list.vala
 *
 * Copyright (C) 2016 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public interface GSvg.DomTransformList : GLib.Object, GXml.Property {

  public abstract int number_of_items { get; }

  public abstract void clear()throws GLib.Error;
  public abstract DomTransform initialize (DomTransform newItem)throws GLib.Error;
  public abstract DomTransform get_item (int index)throws GLib.Error;
  public abstract DomTransform insert_item_before (DomTransform newItem, int index) throws GLib.Error;
  public abstract DomTransform replace_item (DomTransform newItem, int index) throws GLib.Error;
  public abstract DomTransform remove_item (int index)throws GLib.Error;
  public abstract DomTransform append_item (DomTransform newItem)throws GLib.Error;
  public abstract DomTransform create_svg_transform_from_matrix (DomMatrix matrix);
  public abstract DomTransform consolidate () throws GLib.Error;
}
