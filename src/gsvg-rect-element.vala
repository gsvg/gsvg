/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-element.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

public class GSvg.RectElement : GSvg.Transformable,
                          GSvg.DomRectElement, GXml.MappeableElement
{
  // RectElement
  public DomAnimatedLengthX x { get; set; }
  [Description (nick="::x")]
  public AnimatedLengthX mx {
    get { return x as AnimatedLengthX; }
    set { x = value as DomAnimatedLengthX; }
  }
  public DomAnimatedLengthY y { get; set; }
  [Description (nick="::y")]
  public AnimatedLengthY my {
    get { return y as AnimatedLengthY; }
    set { y = value as DomAnimatedLengthY; }
  }
  public DomAnimatedLengthWidth width { get; set; }
  [Description (nick="::width")]
  public AnimatedLengthWidth mwidth {
    get { return width as AnimatedLengthWidth; }
    set { width = value as DomAnimatedLengthWidth; }
  }
  public DomAnimatedLengthHeight height{ get; set; }
  [Description (nick="::height")]
  public AnimatedLengthHeight mheight {
    get { return height as AnimatedLengthHeight; }
    set { height = value as DomAnimatedLengthHeight; }
  }
  public DomAnimatedLengthRX rx { get; set; }
  [Description (nick="::rx")]
  public AnimatedLengthRX mrx {
    get { return rx as AnimatedLengthRX; }
    set { rx = value as DomAnimatedLengthRX; }
  }
  public DomAnimatedLengthRY ry { get; set; }
  [Description (nick="::ry")]
  public AnimatedLengthRY mry {
    get { return ry as AnimatedLengthRY; }
    set { ry = value as DomAnimatedLengthRY; }
  }
  construct {
    initialize ("rect");
  }
  // MappeableElement
  public string get_map_key () { return id; }

  public override bool pick (DomPoint point, double delta) {
    double cx, cy, cwidth, cheight, cstroke;
    cx = cy = cwidth = cheight = cstroke = 0.0;
    var lstroke = stroke_width_to_lenght ();
    if (owner_document is GSvg.DomDocument) {
      var tsvg = ((GSvg.DomDocument) owner_document).root_element;
      if (tsvg != null) {
        cx = tsvg.convert_length_x_pixels (x.base_val);
        cy = tsvg.convert_length_y_pixels (y.base_val);
        cwidth = tsvg.convert_length_x_pixels (width.base_val);
        cheight = tsvg.convert_length_y_pixels (height.base_val);
        cstroke = tsvg.convert_length_x_pixels (lstroke);
      }
    } else {
      cx = x.base_val.value;
      cy = y.base_val.value;
      cwidth = width.base_val.value;
      cheight = height.base_val.value;
      cstroke = lstroke.value;
    }
    if (is_filled ()) {
      if (point.x < (cx + cwidth + delta) && point.x > (cx - delta)
          && point.y < (cy + cheight + delta) && point.y >= (cy - delta)) {
        return true;
      }
    }
    double sw = cstroke / 2;
    double xmx = cx + sw + delta;
    double xmn = cx - sw - delta;
    double ymx = cy + sw + delta;
    double ymn = cy - sw - delta;
    if (point.x < xmx && point.x > xmn && point.y < (ymx + cheight) && point.y > ymn) {
      return true;
    }
    if (point.x < xmx + cwidth && point.x > xmn && point.y < ymx && point.y > ymn) {
      return true;
    }
    if (point.x < xmx + cwidth && point.x > xmn + cwidth && point.y < ymx + cheight && point.y > ymn) {
      return true;
    }
    if (point.x < xmx + cwidth && point.x > xmn && point.y < ymx + cheight && point.y > ymn + cheight) {
      return true;
    }
    return false;
  }
}

