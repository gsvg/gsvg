/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvg-dom-interfaces-basic-shapes.vala
 *
 * Copyright (C) 2016, 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;

namespace GSvg {

public interface DomRectElement : GLib.Object,
                           GXml.DomElement,
                           DomTests,
                           DomLangSpace,
                           DomExternalResourcesRequired,
                           DomStylable,
                           DomTransformable {
  public abstract DomAnimatedLengthX x { get; set; }
  public abstract DomAnimatedLengthY y { get; set; }
  public abstract DomAnimatedLengthWidth width { get; set; }
  public abstract DomAnimatedLengthHeight height { get; set; }
  public abstract DomAnimatedLengthRX rx { get; set; }
  public abstract DomAnimatedLengthRY ry { get; set; }
}

public interface DomRectElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomRectElement get (string id);
  public abstract void append (DomRectElement el);
}

public interface DomCircleElement : GLib.Object,
                             DomElement,
                             DomTests,
                             DomLangSpace,
                             DomExternalResourcesRequired,
                             DomStylable,
                             DomTransformable {
  public abstract DomAnimatedLengthCX cx { get; set; }
  public abstract DomAnimatedLengthCY cy { get; set; }
  public abstract DomAnimatedLengthR  r { get; set; }
}

public interface DomCircleElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomCircleElement get (string id);
  public abstract void append (DomCircleElement el);
}

public interface DomEllipseElement : GLib.Object,
                              DomElement,
                              DomTests,
                              DomLangSpace,
                              DomExternalResourcesRequired,
                              DomStylable,
                              DomTransformable {
  public abstract DomAnimatedLengthCX cx { get; set; }
  public abstract DomAnimatedLengthCY cy { get; set; }
  public abstract DomAnimatedLengthRX rx { get; set; }
  public abstract DomAnimatedLengthRY ry { get; set; }
}

public interface DomEllipseElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomEllipseElement get (string id);
  public abstract void append (DomEllipseElement el);
}

public interface DomLineElement : GLib.Object,
                           DomElement,
                           DomTests,
                           DomLangSpace,
                           DomExternalResourcesRequired,
                           DomStylable,
                           DomTransformable {
  public abstract DomAnimatedLengthX x1 { get; set; }
  public abstract DomAnimatedLengthY y1 { get; set; }
  public abstract DomAnimatedLengthX x2 { get; set; }
  public abstract DomAnimatedLengthY y2 { get; set; }
}

public interface DomLineElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomLineElement get (string id);
  public abstract void append (DomLineElement el);
}

public interface DomAnimatedPoints : GLib.Object {
  public abstract DomPointList points { get; set; }
  public abstract DomPointList animated_points { get; set; }
}

public interface DomPolylineElement : GLib.Object,
                               DomElement,
                               DomTests,
                               DomLangSpace,
                               DomExternalResourcesRequired,
                               DomStylable,
                               DomTransformable,
                               DomAnimatedPoints {
}

public interface DomPolylineElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomPolylineElement get (string id);
  public abstract void append (DomPolylineElement el);
}

public interface DomPolygonElement : GLib.Object,
                              DomElement,
                              DomTests,
                              DomLangSpace,
                              DomExternalResourcesRequired,
                              DomStylable,
                              DomTransformable,
                              DomAnimatedPoints {
}

public interface DomPolygonElementMap : GLib.Object {
  public abstract int length { get; }
  public abstract DomPolygonElement get (string id);
  public abstract void append (DomPolygonElement el);
}

} // GSvg
