/* gsvg-gs-path-element.vala
 *
 * Copyright (C) 2016-2019 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
public class GSvg.PathElement : Transformable, DomAnimatedPathData, DomPathElement
{
  private DomPathSegList _path_seg_list;
  private DomPathSegList _normalized_path_seg_list;
  private DomPathSegList _animated_path_seg_list;
  private DomPathSegList _animated_normalized_path_seg_list;

  [Description (nick="::pathLength")]
  public AnimatedNumber mpath_length { get; set; }


  [Description (nick="::d")]
  public string md { get; set; }
  public string d {
    get { return md; }
    set {
      md = value;
    }
  }

  public DomAnimatedNumber path_length {
    get { return mpath_length as DomAnimatedNumber; }
    construct set {
      if (value is GXml.Property) {
        mpath_length = new AnimatedNumber ();
        mpath_length.value = ((GXml.Property) @value).value;
      }
    }
  }
  construct {
    initialize ("path");
    md = null;
    _path_seg_list = new PathSegList ();
    _normalized_path_seg_list = new PathSegList ();
    _animated_path_seg_list = new PathSegList ();
    _animated_normalized_path_seg_list = new PathSegList ();
  }

  public double get_total_length () {
    return 0.0;
  }
  public DomPoint get_point_at_length (double distance) {
    return new Point ();
  }
  public uint get_path_seg_at_length (double distance) {
    return 0;
  }
  public DomPathSegClosePath create_svg_path_seg_close_path () {
    return new PathSegClosePath ();
  }
  public DomPathSegMovetoAbs create_svg_path_seg_moveto_abs (double x, double y) {
    return new PathSegMovetoAbs ();
  }
  public DomPathSegMovetoRel create_svg_path_seg_moveto_rel (double x, double y) {
    return new PathSegMovetoRel ();
  }
  public DomPathSegLinetoAbs create_svg_path_seg_lineto_abs (double x, double y) {
    return new PathSegLinetoAbs ();
  }
  public DomPathSegLinetoRel create_svg_path_seg_lineto_rel (double x, double y) {
    return new PathSegLinetoRel ();
  }
  public DomPathSegCurvetoCubicAbs
    create_svg_path_seg_curveto_cubic_abs (double x, double y, double x1, double y1, double x2, double y2)
  {
    return new PathSegCurvetoCubicAbs ();
  }
  public DomPathSegCurvetoCubicRel
    create_svg_path_seg_curveto_cubic_rel (double x, double y, double x1, double y1, double x2, double y2)
  {
    return new PathSegCurvetoCubicRel ();
  }
  public DomPathSegCurvetoQuadraticAbs
    create_svg_path_seg_curveto_quadratic_abs (double x, double y, double x1, double y1)
  {
    return new PathSegCurvetoQuadraticAbs ();
  }
  public DomPathSegCurvetoQuadraticRel
    create_svg_path_seg_curveto_quadratic_rel (double x, double y, double x1, double y1)
  {
    return new PathSegCurvetoQuadraticRel ();
  }
  public DomPathSegArcAbs create_svg_path_seg_arc_abs (double x,
                                                    double y,
                                                    double r1,
                                                    double r2,
                                                    double angle,
                                                    bool largeArcFlag,
                                                    bool sweepFlag)
  {
    return new PathSegArcAbs ();
  }
  public DomPathSegArcRel create_svg_path_seg_arc_rel (double x,
                                                    double y,
                                                    double r1,
                                                    double r2,
                                                    double angle,
                                                    bool largeArcFlag,
                                                    bool sweepFlag)
  {
    return new PathSegArcRel ();
  }
  public DomPathSegLinetoHorizontalAbs create_svg_path_seg_lineto_horizontal_abs (double x) {
    return new PathSegLinetoHorizontalAbs ();
  }
  public DomPathSegLinetoHorizontalRel create_svg_path_seg_lineto_horizontal_rel (double x) {
    return new PathSegLinetoHorizontalRel ();
  }
  public DomPathSegLinetoVerticalAbs create_svg_path_seg_lineto_vertical_abs (double y) {
    return new PathSegLinetoVerticalAbs ();
  }
  public DomPathSegLinetoVerticalRel create_svg_path_seg_lineto_vertical_rel (double y) {
    return new PathSegLinetoVerticalRel ();
  }
  public DomPathSegCurvetoCubicSmoothAbs
    create_svg_path_seg_curveto_cubic_smooth_abs (double x,
                                                  double y,
                                                  double x2,
                                                  double y2)
  {
    return new PathSegCurvetoCubicSmoothAbs ();
  }
  public DomPathSegCurvetoCubicSmoothRel
    create_svg_path_seg_curveto_cubic_smooth_rel (double x,
                                                  double y,
                                                  double x2,
                                                  double y2)
  {
    return new PathSegCurvetoCubicSmoothRel ();
  }
  public DomPathSegCurvetoQuadraticSmoothAbs
    create_svg_path_seg_curveto_quadratic_smooth_abs (double x, double y)
  {
    return new PathSegCurvetoQuadraticSmoothAbs ();
  }
  public DomPathSegCurvetoQuadraticSmoothRel
    create_svg_path_seg_curveto_quadratic_smooth_rel (double x, double y)
  {
    return new PathSegCurvetoQuadraticSmoothRel ();
  }

  // AnimatedPathData
  public DomPathSegList path_seg_list {
    get {
      if (md != null) {
        try {
            _path_seg_list.parse (md);
        } catch (GLib.Error e) {
            warning ("Error parsing path: %s", e.message);
        }
      }
      return _path_seg_list;
    }
  }
  public DomPathSegList normalized_path_seg_list {
    get {
      if (md != null) {
        try {
            _normalized_path_seg_list.parse (md);
        } catch (GLib.Error e) {
            warning ("Error parsing path: %s", e.message);
        }
      }
      return _normalized_path_seg_list;
    }
  }
  public DomPathSegList animated_path_seg_list {
    get {
      if (md != null) {
        try {
            _animated_path_seg_list.parse (md);
        } catch (GLib.Error e) {
            warning ("Error parsing path: %s", e.message);
        }
      }
      return _animated_path_seg_list;
    }
  }
  public DomPathSegList animated_normalized_path_seg_list {
    get {
      if (md != null) {
        try {
            _animated_normalized_path_seg_list.parse (md);
        } catch (GLib.Error e) {
            warning ("Error parsing path: %s", e.message);
        }
      }
      return _animated_normalized_path_seg_list;
    }
  }
}


public class GSvg.PathElementMap : GXml.HashMap, DomPathElementMap {
  public int length { get { return ((GXml.HashMap) this).length; } }
  construct {
    try {
      initialize (typeof (PathElement));
    } catch (GLib.Error e) { warning ("Error: "+e.message); }
  }
  public new DomPathElement DomPathElementMap.get (string id) {
    return ((GXml.HashMap) this).get (id) as DomPathElement;
  }
  public new void append (DomPathElement el) {
    try {
      ((GXml.HashMap) this).append (el);
    } catch (GLib.Error e) {
      warning ("Error adding object to paths collection: %s", e.message);
    }
  }
}

