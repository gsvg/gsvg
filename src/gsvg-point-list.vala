/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* css-classes.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using Gee;

public class GSvg.PointList : Gee.ArrayList<DomPoint>,
                                    GXml.Property,
                                    DomPointList
{
  private string _value = null;
  private bool modified = false;

  public int number_of_items { get { return size; } }

  public new void clear () throws GLib.Error { this.clear (); }
  public DomPoint initialize (DomPoint new_item) throws GLib.Error {
    add (new_item);
    modified = true;
    return new_item;
  }
  public DomPoint get_item (int index) throws GLib.Error {
    return get (index);
  }
  public DomPoint insert_item_before (DomPoint new_item, int index) throws GLib.Error {
    insert (index, new_item);
    modified = true;
    return new_item;
  }
  public DomPoint replace_item (DomPoint new_item, int index) throws GLib.Error {
    remove_at (index);
    insert (index, new_item);
    modified = true;
    return new_item;
  }
  public DomPoint remove_item (int index) throws GLib.Error {
    modified = true;
    return remove_at (index);
  }
  public DomPoint append_item (DomPoint new_item) throws GLib.Error {
    modified = true;
    add (new_item);
    return new_item;
  }
  public string? value {
    set {
      _value = value;
      string s = _value.replace (",", " ");
      s = s.replace ("(", "");
      s = s.replace (")", "");
      string unparsed = s;
      while (unparsed != null) {
        var p = new Point ();
        double x, y;
        x = y = 0.0;
        message (unparsed);
        unparsed = DomPoint.parse_point_values (unparsed, out x, out y);
        if (unparsed == null) {
          break;
        }
        p.x = x;
        p.y = y;
        add (p);
      }
    }
    owned get {
      if (_value == null && number_of_items > 0 || modified) {
        _value = to_string ();
      }
      return _value;
    }
  }
  public bool validate_value (string? val) {
    return "," in val; // FIXME
  }
}

